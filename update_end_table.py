from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf
import os
import logging
import argparse
import fpa_dataprep.spark_utils as su


def load_files_to_dataframe(file_path, schema):
    """
    hdp files can include wild card *
    """
    try:
        df = spark.read.format("com.databricks.spark.csv") \
            .options(header='true') \
            .load(file_path, schema=schema)
        return df
    except Exception as e:
        logger.error(e, exc_info=True)


def write_raw_fpa_scores(data_dir):
    from fpa_dataprep.constant import FINAL_FPA_SCORE_RAW, FINAL_FPA_SCORE_RAW_SCHEMA
    try:
        logger.info(f"[func write_raw_fpa_scores] Reading files from {data_dir}/*_fpa_score_raw.csv ...")
        df = load_files_to_dataframe(file_path=os.path.join(data_dir, "*_fpa_score_raw.csv"), 
                                     schema=FINAL_FPA_SCORE_RAW_SCHEMA)
        su.save_table(df, FINAL_FPA_SCORE_RAW)
    except Exception as e:
        logger.error(e, exc_info=True)


def write_ranked_fpa_scores(data_dir):
    from fpa_dataprep.constant import FINAL_FPA_SCORE_RANKED, FINAL_FPA_SCORE_RANKED_SCHEMA
    try:
        logger.info(f"[func write_ranked_fpa_scores] Reading files from {data_dir}/*_fpa_score_ranked.csv ...")
        df = load_files_to_dataframe(file_path=os.path.join(data_dir, "*_fpa_score_ranked.csv"), 
                                     schema=FINAL_FPA_SCORE_RANKED_SCHEMA)
        su.save_table(df, FINAL_FPA_SCORE_RANKED)
    except Exception as e:
        logger.error(e, exc_info=True)


if __name__ == "__main__":
    # setting up spark and koalas
    spark = SparkSession \
        .builder \
        .enableHiveSupport() \
        .appName("fpa_update_end_table") \
        .getOrCreate()
    sc = SparkContext.getOrCreate(SparkConf().setMaster("local[*]"))

    # parsing args
    parser = argparse.ArgumentParser()
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--data_dir', type=str, action='store', dest='data_dir',
                        help="Relative location with respect to home_dir, indicating where to find all the "
                             "feature data.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="log/fpa_data_prep.log",
                        help="The file location for the log, with respect to home_dir. [Default] 'log/fpa_data_prep.log'.")


    args = parser.parse_args()

    data_dir = os.path.join(os.path.realpath(args.home_dir), args.data_dir)
    logfile_name = os.path.join(os.path.realpath(args.home_dir), args.logfile_name)

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # ======== START OF MAIN PROGRAM ========
    # add python module to drivers
    su.add_user_module(sc)

    try:
        write_raw_fpa_scores(data_dir)
        write_ranked_fpa_scores(data_dir)
        logger.info("Success!")
    except Exception as e:
        logger.error(e, exc_info=True)
    
    spark.stop()
