set -e

dtstring=$1
logfile="log/fpa_$(date +"%Y%m%d%H%M%S").log"

# refresh datasets
python data_prep_main.py --mode score --dt $dtstring --logfilename $logfile &&

# scoring
echo "==============================" >> $logfile &&
python score.py --dt $dtstring --logfilename $logfile &&

# load scores back to cip
echo "==============================" >> $logfile &&
python update_end_table.py --data_dir scores/$dtstring --logfilename $logfile &&

# load scores to HDFS (to be used by GCS project)
echo "Copying ranked files to HDFS ..." >> $logfile &&
hadoop fs -copyFromLocal -f scores/$dtstring/*_ranked.csv abfs://data@clouderastorageaccountpd.dfs.core.windows.net/tmp/fpa2gcs &&

echo "Success!" >> $logfile
