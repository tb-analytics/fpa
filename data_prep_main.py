from pyspark.sql import SparkSession
from pyspark import SparkContext, SparkConf
import databricks.koalas as ks
import pandas as pd
import numpy as np
import os
import warnings
import logging
import argparse
from datetime import date
import fpa_dataprep.spark_utils as su
from fpa_dataprep.get_raw_data import refresh_all_regions_data
from fpa_dataprep.feature_extractions import get_all_features, datasets_for_scoring, datasets_for_training


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":
    # setting up spark and koalas
    spark = SparkSession \
        .builder \
        .enableHiveSupport() \
        .appName("fpa_data_prep") \
        .getOrCreate()
    sc = SparkContext.getOrCreate(SparkConf().setMaster("local[*]"))

    spark.conf.set("spark.sql.execution.arrow.enabled", True)
    ks.set_option("compute.ops_on_diff_frames", True)
    ks.set_option("compute.default_index_type", "distributed")  # Use default index prevent overhead.
    warnings.filterwarnings("ignore")

    # parsing args
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, action='store', dest='mode', default='score',
                        help="Mode for modeling behaviors. [Values] 'score', 'train'. "
                             "[Default] 'score'.")
    parser.add_argument('--dt', type=str, action='store', dest='dt', default=date.today().__str__(),
                        help="Date string for the refresh date. [Default] today's date.")
    parser.add_argument('--regions', nargs='+', action='store', dest='regions', default=['NA', 'EU', 'AS', 'JP'],
                        help="A list of global regions to be included in the refresh. [Values] a list within"
                             " 'NA', 'EU', 'AS', 'JP'. [Default] all regions.")
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--output_data_dir', type=str, action='store', dest='output_data_dir', default='data',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'data'.")
    parser.add_argument('--metadata_dir', type=str, action='store', dest='metadata_dir', default='metadata',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'metadata'.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="log/fpa_data_prep.log",
                        help="The file location for the log, with respect to home_dir. [Default] 'log/fpa_data_prep.log'.")
    parser.add_argument('--refresh_all_data', type=str2bool, action='store',
                        dest='refresh_all_data', default=True, help="Whether to refresh all trans/email data. "
                                                                    "[Default] True")
    parser.add_argument('--transform_features', type=str2bool, action='store', dest='transform_features', default=True,
                        help="Whether to transform features to numpy array.")

    args = parser.parse_args()

    dt = pd.Timestamp(args.dt, freq='MS')
    mode = args.mode.lower()
    regions = args.regions    
    target = "fp_pct_target"

    output_data_dir = su.make_dir_if_not_exist(os.path.join(os.path.realpath(args.home_dir), args.output_data_dir))
    metadata_dir = su.make_dir_if_not_exist(os.path.join(os.path.realpath(args.home_dir), args.metadata_dir))
    logfile_name = os.path.join(su.make_dir_if_not_exist(os.path.realpath(args.home_dir)), args.logfile_name)
    refresh_all_data = args.refresh_all_data

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    # ======== START OF MAIN PROGRAM ========
    # add python module to drivers
    su.add_user_module(sc)

    if refresh_all_data:
        refresh_all_regions_data(spark, sc, dt)

    try:
        for region in regions:
            logger.info(f"[func main] Getting {region} feature data ...")
            features_df, feat_sets, n_cat = get_all_features(spark, dt, target, region, mode)
            
            if args.transform_features:
                logger.info(f"[func main] Transforming {region} features ...")
                if mode == "train":
                    datasets_gen = datasets_for_training(features_df, feat_sets, n_cat, target)
                elif mode == "score":
                    chunk = 1
                    if region == "NA":
                        chunk = 10
                    datasets_gen = datasets_for_scoring(features_df, feat_sets, n_cat, target, chunks=chunk)
                else:
                    raise ValueError("[func main] mode can only be either `train` or `score`.")
                
                for part, datasets in enumerate(datasets_gen):
                    saving_path = su.make_dir_if_not_exist(os.path.join(output_data_dir, region, mode, args.dt))
                    logger.info(f"[func main] Saving {region} part {part:04} to {saving_path}...")
                    np.savez_compressed(os.path.join(saving_path, f"fpa_datasets_{part:04}.npz"), **datasets)
                    logger.info(f"[func main] {region} part {part:04} saved at {saving_path}.")

        logger.info(f"[func main] All done.")
    
    except Exception as e:
        logger.error(e, exc_info=True)
