import torch
from torch.utils.data import DataLoader, TensorDataset
from ptframework.get_model import *
from ptframework.data_prep import *
from ptframework.utils import make_dir_if_not_exist
import os
import itertools
from datetime import datetime
from collections import OrderedDict
import logging
import argparse


if __name__ == '__main__':

    # parsing args
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, action='store', dest='data_dir',
                        help="Relative location with respect to home_dir, indicating where to find all the "
                             "training data.")
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--model_output_dir', type=str, action='store', dest='model_output_dir', default='models',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "models. [Default] 'models'.")
    parser.add_argument('--metadata_dir', type=str, action='store', dest='metadata_dir', default='metadata',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "results/scores/profiles. [Default] 'metadata'.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="log/fpa_model_train.log",
                        help="The file location for the log, with respect to home_dir. [Default] 'log/fpa_model_train.log'.")

    args = parser.parse_args()

    today = datetime.today().strftime("%Y%m%d")
    data_dir = os.path.join(os.path.realpath(args.home_dir), args.data_dir)
    model_output_dir = make_dir_if_not_exist(os.path.join(os.path.realpath(args.home_dir), args.model_output_dir, today))
    metadata_dir = make_dir_if_not_exist(os.path.join(os.path.realpath(args.home_dir), args.metadata_dir, today))
    logfile_name = os.path.join(os.path.realpath(args.home_dir), args.logfile_name)
    
    # setup logger
    logging_level = logging.INFO
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging_level)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging_level)
    logger.addHandler(handler)

    try:
        n_cat = 21
        n_feat = 88  # 75 for JP, 88 for others

        param_grid = OrderedDict(
            {
                "batch_size": [500],
                "lr": [0.0005],
                "kernel_size": [5],
                "dropout": [0.],
                # "tag": ["ignore0_no_weights", "ignore0_amp_range_0.5+_2", "ignore0_amp_range_0.8+_5"]
                "tag": ["ignore0_amp_range_0.8+_2"]
            }
        )
        for batch_size, lr, kernel_size, dropout, tag in itertools.product(*param_grid.values()):
            logger.info("=~"*30)
            train_loader = DataLoader(FPAIterableDataset(data_dir, "train"),
                           batch_size=batch_size, num_workers=4, worker_init_fn=FPAIterableDataset.worker_init_fn)
            test_loader = DataLoader(FPAIterableDataset(data_dir, "test"),
                                     batch_size=batch_size, num_workers=4, worker_init_fn=FPAIterableDataset.worker_init_fn)
            
            model = get_cnn_model(n_cat, n_feat, train_loader, test_loader, lr=lr,
                                  kernel_size=kernel_size, dropout=dropout,
                                  log_model_notes=tag + f"_{args.model_output_dir[-2:]}", loss_weight_method=tag)

            model.add_early_stopping_callback(monitor="rmse", min_delta=.0001, patience=10)
            model.add_best_checkpoint_callback(monitor="rmse", saving_path=model_output_dir)
            model.add_end_of_training_eval(os.path.join(metadata_dir, "eval_figs"))
            model.trainer.run(train_loader, max_epochs=200)
    except Exception as e:
        logger.error(e, exc_info=True)
