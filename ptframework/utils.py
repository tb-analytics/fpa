import importlib
import os
from IPython.display import display
import numpy as np
from typing import Dict
import matplotlib.pyplot as plt
import sparse
import logging
logger = logging.getLogger(__name__)


def func_constructor(func_configs):
    _module_name = func_configs['module']
    _func_name = func_configs['func']
    if 'params' in func_configs:
        _func_params = func_configs['params']
    else:
        _func_params = dict()

    _module = importlib.import_module(_module_name)
    _func = getattr(_module, _func_name)

    func = _func(**_func_params)
    return func


def make_dir_if_not_exist(pathdir):
    if not os.path.exists(pathdir):
        os.makedirs(pathdir)
    return pathdir


def write_list_to_file(pathdir, lst):
    with open(pathdir, 'w+') as filehandle:
        filehandle.writelines("%s\n" % ele for ele in lst)
    return


def save_plot(fig, savepath=None, figname=None):
    if savepath:
        savepath = make_dir_if_not_exist(savepath)
        try:
            fig.savefig(os.path.join(savepath, figname+'.png'), dpi=300, bbox_inches='tight')
        except:
            fig.savefig(os.path.join(savepath, figname+'.png'), dpi=300)
        plt.close(fig)
    else:
        display(fig)


def save_dataframe(df, path, filename, *args, **kwargs):
    path = make_dir_if_not_exist(path)
    save_path = os.path.join(path, filename)
    df.to_csv(save_path, *args, **kwargs)
    logger.info(f"[func save_dataframe] {save_path} saved.")
    return



def dense_to_coo(mat: np.ndarray) -> Dict[str, np.ndarray]:
    mat_s = sparse.COO(mat)
    data_dict = {
        "data": mat_s.data,
        "shape": mat_s.shape,
        "fill_value": mat_s.fill_value,
        "coords": mat_s.coords
    }
    
    return data_dict


def coo_to_dense(data_dict: Dict[str, np.ndarray]) -> sparse.COO:
    coords = data_dict["coords"]
    data = data_dict["data"]
    shape = tuple(data_dict["shape"])
    fill_value = data_dict["fill_value"][()]
    
    dense_mat =  sparse.COO(
        coords=coords,
        data=data,
        shape=shape,
        sorted=True,
        has_duplicates=False,
        fill_value=fill_value,
    ).todense()

    return dense_mat
