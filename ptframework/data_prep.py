import torch
from torch.utils.data import IterableDataset
import numpy as np
from typing import Optional, List
from pathlib import Path
import logging

logger = logging.getLogger(__name__)


def _read_sparse_X(arr: np.ndarray) -> np.ndarray:
    from ptframework.utils import coo_to_dense
    if (arr.dtype == 'O') and (arr.size == 1):
        datadict = arr.item()
        dense_arr = coo_to_dense(datadict)
    else:
        dense_arr = arr

    return np.nan_to_num(dense_arr)


def standardize(tensor: torch.Tensor, dimension: int = 1, skip_cols: Optional[List[int]] = None) -> torch.Tensor:
    logger.debug(f"[func standardize] Standardizing tensor along dimension {dimension}.")
    all_cols_idx = np.ones_like(np.arange(tensor.shape[1]), bool)
    if skip_cols:
        all_cols_idx[skip_cols] = False

    means = tensor[:, all_cols_idx].mean(dim=dimension, keepdim=True)
    stds = tensor[:, all_cols_idx].std(dim=dimension, keepdim=True)
    tensor[:, all_cols_idx] = (tensor[:, all_cols_idx] - means) / stds
    return torch.nan_to_num(tensor)


def prep_sparse_tensor(x: np.ndarray, n_feat: int):
    """
    :param x: should be a (N_values, 3) dimension np array, where represents row_idx, col_idx, val respectively
    :param n_feat:
    :return:
    """
    logger.debug("[func prep_sparse_tensor] Converting (row, col, val) tensor to sparse tensor (in dense format).")
    x = x[x[:, 1] < n_feat]

    row_idx = x[:, 0].astype(np.int32)
    indices = torch.tensor(x[:, :2].astype(np.int32).T)
    val = x[:, 2].astype(float)

    M_sample = int(row_idx.max() + 1)
    sparse_tensor = torch.sparse_coo_tensor(indices, val,
                                            size=(M_sample, n_feat),
                                            dtype=torch.float
                                            )
    sparse_tensor = sparse_tensor.to_dense()
    sparse_tensor = sparse_tensor[sparse_tensor.abs().sum(dim=1).bool()]  # drop all 0s rows
    return sparse_tensor


def sampling(sample_rate, X: np.ndarray, y: Optional[np.ndarray] = None):
    import math
    import random
    pool = np.unique(X[:, 0]).shape[0]
    sample_size = math.ceil(pool * sample_rate)
    idx = random.sample(range(pool), sample_size)
    logger.debug(f"[class FPAIterableDataset.sampling] fetching {sample_size} samples")
    _X = X[np.isin(X[:, 0], idx)]
    if y is not None:
        _y = y[idx]
        return _X, _y
    else:
        return _X, None


def prep_features_from_file(file_name, x_label: str, 
                            y_label: Optional[str] = None,
                            n_feat_label: Optional[str] = None,
                            n_feat: Optional[int] = None, 
                            sample_rate: Optional[float] = None,
                            standarized: bool = True,
                            omit_features: Optional[List[int]] = None):
    with np.load(file_name, allow_pickle=True) as fh:
        logger.debug(f"[func prep_features_from_file] Processing {file_name} ...")
        data_repo_keys = list(fh.keys())
        if n_feat_label and n_feat_label in data_repo_keys:
            _n_feat = fh[n_feat_label]
        elif isinstance(n_feat, int):
            _n_feat = n_feat
        else:
            raise ValueError("[func prep_features_from_file] Either n_feat_label[str] or n_feat[int] should be valid.")

        _x = _read_sparse_X(fh[x_label])

        if omit_features is not None:
            _x = _x[~np.isin(_x[:, 1], np.array(omit_features))]

        if y_label:
            _y = fh[y_label]
            _y = np.nan_to_num(_y)
        else:
            _y = None
        
        if sample_rate:
            _x, _y = sampling(sample_rate, _x, _y)

        _xt = prep_sparse_tensor(_x, _n_feat)
        if standarized:
            _xt = standardize(_xt)
        
        if _y is not None:
            _yt = torch.tensor(_y, dtype=torch.float)
            return _xt, _yt
        else:
            return _xt


def prep_features_from_file_cnn(file_name, x_label:str, y_label:Optional[str] = None):
    with np.load(file_name, allow_pickle=True) as fh:
        logger.debug(f"[func prep_features_from_file_cnn] Processing {file_name} ...")
        _x = _read_sparse_X(fh[x_label])
        _xt = torch.tensor(_x, dtype=torch.float)

        if y_label:
            _y = fh[y_label]
            _y = np.nan_to_num(_y)
            _yt = torch.tensor(_y, dtype=torch.float)
            return _xt, _yt
        else:
            return _xt


class FPAIterableDataset(IterableDataset):
    def __init__(self, file_dir, data_label, file_ext: str = "npz", n_feat: Optional[int] = None,
                 sample_rate: float = 1.0, standarized: bool = True,
                 omit_features: Optional[List[int]] = None):
        self.files = list(Path(file_dir).rglob(f"*.{file_ext}"))
        self.data_label = data_label
        self.n_feat = n_feat
        self.sample_rate = sample_rate
        self.standarized = standarized
        self.omit_features = omit_features

    def shuffling(self, X: torch.Tensor, y: torch.Tensor):
        import random
        total_size = X.size(0)
        sampled_idx = list(range(total_size))
        random.shuffle(sampled_idx)
        logger.debug(f"[class FPAIterableDataset.shuffling] shuffling...")
        for i in sampled_idx:
            yield X[i], y[i]

    def parse_file(self):
        import random
        # alternating the order a file is being read
        for file_name in random.sample(self.files, len(self.files)):
            logger.debug(f"[class FPAIterableDataset.parse_file] parsing {file_name}")
            # X, y = prep_features_from_file(file_name, f"{self.data_label}_X", f"{self.data_label}_y",
            #                                n_feat=self.n_feat, sample_rate=self.sample_rate, standarized=self.standarized,
            #                                omit_features=self.omit_features)
            X, y = prep_features_from_file_cnn(file_name, f"{self.data_label}_X", f"{self.data_label}_y")
            assert all([isinstance(d, torch.Tensor) for d in [X, y]]), "[class FPAIterableDataset.parse_file] X and y must all be tensors."
            yield from self.shuffling(X, y)

    def __iter__(self):
        return self.parse_file()

    @staticmethod
    def worker_init_fn(worker_id):
        import math
        worker_info = torch.utils.data.get_worker_info()
        worker_id = worker_info.id
        total_workers = worker_info.num_workers
        dataset = worker_info.dataset  # the dataset copy in this worker process

        total_files = dataset.files
        num_files = math.ceil(len(total_files) / total_workers)
        worker_files = total_files[worker_id*num_files:(worker_id+1)*num_files]
        dataset.files = worker_files
        return
