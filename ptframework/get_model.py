from ptframework.model import GetModel, FactorizationMachines2D, LinearRegression, MLP, CNN
import torch
from torch import nn
from datetime import datetime
from ignite.metrics import MeanSquaredError, RootMeanSquaredError, MeanAbsoluteError
import os
from typing import Any, Callable, Dict, Optional, Sequence, Tuple, Union
import logging

logger = logging.getLogger(__name__)


def get_fm2d_model(n_feat: int, train_loader: torch.utils.data.DataLoader,
                   val_loader: torch.utils.data.DataLoader, lr: float, k: int,
                   reg_alpha_feat: float, log_model_notes: Optional[str] = None,
                   loss_weight_method: Optional[str] = None) -> GetModel:
    hyperparam = {
        "lr": lr,
        "weight_method_tag": loss_weight_method
    }

    modelparam = {
        "n_feat": n_feat,
        "k": k,
        "reg_param": reg_alpha_feat
    }

    modelparam_str = "_".join(f"{k}={v}" for k, v in modelparam.items())
    hyperparam_str = "_".join(f"{k}={v}" for k, v in hyperparam.items())

    if loss_weight_method:
        model_base = FactorizationMachines2D.weighted_loss_model(**modelparam, **hyperparam)
    else:
        model_base = FactorizationMachines2D(**modelparam)

    model = GetModel(model=model_base,
                     modelparam_str=modelparam_str,
                     hyperparam_str=hyperparam_str,
                     train_loader=train_loader,
                     val_loader=val_loader,
                     optimizer=torch.optim.Adam(model_base.parameters(), lr=lr),
                     criterion=model_base.loss,
                     eval_metrics={'mse': MeanSquaredError(),
                                   'rmse': RootMeanSquaredError(),
                                   'mae': MeanAbsoluteError()},
                     logging_params={
                         'log_dir': os.path.join("runs", datetime.now().strftime("%Y_%m_%d"),
                                                 f"{str(datetime.now())}_{repr(model_base)}_{modelparam_str}_{hyperparam_str}"),
                         'log_model_notes': log_model_notes,
                         'log_iter_interval': 500})
    return model


def get_lr_model(n_feat: int, train_loader: torch.utils.data.DataLoader,
                 val_loader: torch.utils.data.DataLoader, lr: float,
                 reg_alpha_feat: float, log_model_notes: Optional[str] = None,
                 loss_weight_method: Optional[str] = None) -> GetModel:
    hyperparam = {
        "lr": lr,
        "weight_method_tag": loss_weight_method
    }

    modelparam = {
        "n_feat": n_feat,
        "reg_param": reg_alpha_feat
    }
    modelparam_str = "_".join(f"{k}={v}" for k, v in modelparam.items())
    hyperparam_str = "_".join(f"{k}={v}" for k, v in hyperparam.items())

    if loss_weight_method:
        model_base = LinearRegression.weighted_loss_model(**modelparam, **hyperparam)
    else:
        model_base = LinearRegression(**modelparam)

    model = GetModel(model=model_base,
                     modelparam_str=modelparam_str,
                     hyperparam_str=hyperparam_str,
                     train_loader=train_loader,
                     val_loader=val_loader,
                     optimizer=torch.optim.Adam(model_base.parameters(), lr=lr),
                     criterion=model_base.loss,
                     eval_metrics={'mse': MeanSquaredError(),
                                   'rmse': RootMeanSquaredError(),
                                   'mae': MeanAbsoluteError()},
                     logging_params={
                         'log_dir': os.path.join("runs", datetime.now().strftime("%Y_%m_%d"),
                                                 f"{str(datetime.now())}_{repr(model_base)}_{modelparam_str}_{hyperparam_str}"),
                         'log_model_notes': log_model_notes,
                         'log_iter_interval': 500})
    return model


def get_mlp_model(n_feat: int, train_loader: torch.utils.data.DataLoader,
                  val_loader: torch.utils.data.DataLoader, lr: float,
                  dropout: float = 0.1,
                  log_model_notes: Optional[str] = None,
                  loss_weight_method: Optional[str] = None) -> GetModel:
    hyperparam = {
        "lr": lr,
        "weight_method_tag": loss_weight_method
    }

    modelparam = {
        "n_feat": n_feat,
        "dropout": dropout
    }
    modelparam_str = "_".join(f"{k}={v}" for k, v in modelparam.items())
    hyperparam_str = "_".join(f"{k}={v}" for k, v in hyperparam.items())

    if loss_weight_method:
        model_base = MLP.weighted_loss_model(**modelparam, **hyperparam)
    else:
        model_base = MLP(**modelparam)

    model = GetModel(model=model_base,
                     modelparam_str=modelparam_str,
                     hyperparam_str=hyperparam_str,
                     train_loader=train_loader,
                     val_loader=val_loader,
                     optimizer=torch.optim.Adam(model_base.parameters(), lr=lr),
                     criterion=model_base.loss,
                     eval_metrics={'mse': MeanSquaredError(),
                                   'rmse': RootMeanSquaredError(),
                                   'mae': MeanAbsoluteError()},
                     logging_params={
                         'log_dir': os.path.join("runs", datetime.now().strftime("%Y_%m_%d"),
                                                 f"{str(datetime.now())}_{repr(model_base)}_{modelparam_str}_{hyperparam_str}"),
                         'log_model_notes': log_model_notes,
                         'log_iter_interval': 500})
    return model


def get_cnn_model(n_cat: int, n_feat:int, train_loader: torch.utils.data.DataLoader,
                  val_loader: torch.utils.data.DataLoader, lr: float,
                  kernel_size: int = 3,
                  dropout: float = 0.1,
                  log_model_notes: Optional[str] = None,
                  loss_weight_method: Optional[str] = None) -> GetModel:

    def reduce_dimension_f(t):
        y_pred = t[0].detach().flatten()
        y = t[1].detach().flatten()

        # remove 0s in calculations
        idx = (y > 0.01).nonzero(as_tuple=True)

        return y_pred[idx], y[idx]

    hyperparam = {
        "lr": lr,
        "weight_method_tag": loss_weight_method
    }

    modelparam = {
        "n_cat": n_cat,
        "n_feat": n_feat,
        "kernel_size": kernel_size,
        "dropout": dropout
    }
    modelparam_str = "_".join(f"{k}={v}" for k, v in modelparam.items())
    hyperparam_str = "_".join(f"{k}={v}" for k, v in hyperparam.items())

    if loss_weight_method:
        model_base = CNN.weighted_loss_model(**modelparam, **hyperparam)
    else:
        model_base = CNN(**modelparam)

    model = GetModel(model=model_base,
                     modelparam_str=modelparam_str,
                     hyperparam_str=hyperparam_str,
                     train_loader=train_loader,
                     val_loader=val_loader,
                     optimizer=torch.optim.Adam(model_base.parameters(), lr=lr),
                     criterion=model_base.loss,
                     eval_metrics={'mse': MeanSquaredError(output_transform=reduce_dimension_f),
                                   'rmse': RootMeanSquaredError(output_transform=reduce_dimension_f),
                                   'mae': MeanAbsoluteError(output_transform=reduce_dimension_f)},
                     logging_params={
                         'log_dir': os.path.join("runs", datetime.now().strftime("%Y_%m_%d"),
                                                 f"{str(datetime.now())}_{repr(model_base)}_{modelparam_str}_{hyperparam_str}"),
                         'log_model_notes': log_model_notes,
                         'log_iter_interval': 500})
    return model
