import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import torch
from torch import nn
from ignite.engine import Events, Engine, create_supervised_trainer, create_supervised_evaluator
from ignite.handlers import EarlyStopping, Checkpoint, DiskSaver, global_step_from_engine
from ignite.metrics import Metric
from tensorboardX import SummaryWriter
from ptframework.evaluations import *
from ptframework.utils import save_plot
from datetime import datetime
import os
from typing import Any, Callable, Dict, Optional, Sequence, Tuple, Union
import logging

logger = logging.getLogger(__name__)


def weighted_mse(y_pred, y_true, weights):
    err = torch.pow(y_pred.squeeze() - y_true.squeeze(), 2) * weights.squeeze()
    return torch.sum(err) / err.size().numel()


def general_sigmoid(x: torch.Tensor, a: float = 1.0, tau: float = 0.0):
    y = 1 / (1 + torch.exp(-a * (x - tau)))
    return y


class NNModule(nn.Module):  # a wrapper class to implement a function to calculate loss weights across all models
    def __init__(self, weight_method_tag="no_weights",  *args, **kwargs):
        super().__init__()
        self.weight_method_tag = weight_method_tag

    def loss_weights(self, arr: torch.Tensor):
        if self.weight_method_tag.startswith("ignore0_"):
            weight_string = self.weight_method_tag[8:]
            remove_0_ws = True
        else:
            weight_string = self.weight_method_tag
            remove_0_ws = False

        if weight_string.startswith("sin_weights_"):
            _w = int(weight_string[12:])
            weights = torch.sin(arr.squeeze()) * _w
        elif weight_string.startswith("sig_weights_"):
            _w = int(weight_string[12:])
            weights = general_sigmoid(arr, 30, .6) * _w
        elif weight_string == "no_weights":
            weights = torch.ones_like(arr)
        elif weight_string == "y_true_weights":
            weights = arr
        elif weight_string.startswith("amp_range_"):
            threshold = float(weight_string[10:13])
            _w = int(weight_string[15:])
            weights = torch.ones_like(arr)
            idx = (arr >= threshold).nonzero(as_tuple=True)
            weights[idx] = _w
        else:
            raise ValueError()

        if remove_0_ws:
            idx = (arr <= 0.01).nonzero(as_tuple=True)  # due to transform 0 is transfromed as 0.0024
            weights[idx] = 0.0
            return weights
        else:
            return weights
    
    @classmethod
    def reload(cls, model_dir, mode="score", **modelparams):
        model = cls(**modelparams)
        model.load_state_dict(torch.load(model_dir))
        if mode == "score":
            model.eval()
        elif mode == "train":
            model.train()
        else:
            raise ValueError("[class NNModule.reload] mode can only be either `train` or `score`.")
        return model


class FactorizationMachines2D(NNModule):

    def __init__(self, n_feat: int, k: int = 10, reg_param: float = .01):
        """
        # Initially we fill V with random values sampled from Gaussian distribution
        # NB: use nn.Parameter to compute gradients
        :param n_feat: number of features
        :param k: latent factor dimensions
        """
        super().__init__()
        self.V = nn.Parameter(torch.rand(n_feat, k), requires_grad=True)
        self.linear = nn.Linear(n_feat, 1)
        self.n_feat = n_feat
        self.k = k
        self.reg_param = reg_param

    def __str__(self):
        return f"Factorization Machines 2-way model with {self.n_feat} features and latent vector of dim={self.k}."

    def __repr__(self):
        return f"FactorizationMachines2D(n_feat={self.n_feat}, k={self.k})"

    @classmethod
    def weighted_loss_model(cls, n_feat: int, k: int = 10, reg_param: float = .01, weight_method_tag: str = "no_weights", *args, **kwargs):
        new_cls = cls(n_feat, k, reg_param)
        new_cls.weight_method_tag = weight_method_tag
        return new_cls

    def forward(self, _x: torch.Tensor) -> torch.Tensor:
        """
        Follow S Rendle's paper to calculate interaction part in linear time.
        :param _x: feature tensor
        :return: prediction tensor of shape (M_sample, 1)
        """
        logger.debug(f"[{self.__repr__()}] calculating forward pass ...")
        x = _x.clone().detach()
        out_1 = torch.matmul(x, self.V).pow(2).sum(1, keepdim=True)
        out_2 = torch.matmul(x.pow(2), self.V.pow(2)).sum(1, keepdim=True)
        interactions_part = out_1 + out_2
        linear_part = self.linear(x)
        prediction = linear_part + interactions_part
        trans_prediction = torch.sigmoid(prediction.squeeze())

        return trans_prediction

    def loss(self, prediction, target):
        loss_weights = self.loss_weights(target)
        mse_loss = weighted_mse(prediction.squeeze(), target.squeeze(), loss_weights)
        fm_reg_loss = torch.linalg.norm(self.V) * self.reg_param
        linear_reg_loss = torch.linalg.norm(self.linear.weight) * self.reg_param
        return mse_loss + fm_reg_loss + linear_reg_loss


class LinearRegression(NNModule):

    def __init__(self, n_feat: int, reg_param: float = .01):
        """
        # Initially we fill V with random values sampled from Gaussian distribution
        # NB: use nn.Parameter to compute gradients
        :param n_feat: number of features
        :param k: latent factor dimensions
        """
        super().__init__()
        self.linear = nn.Linear(n_feat, 1)
        self.n_feat = n_feat
        self.reg_param = reg_param

    def __str__(self):
        return f"LinearRegression model with {self.n_feat} features."

    def __repr__(self):
        return f"LinearRegression(n_feat={self.n_feat})"

    @classmethod
    def weighted_loss_model(cls, n_feat: int, reg_param: float = .01,
                            weight_method_tag: str = "no_weights", *args, **kwargs):
        new_cls = cls(n_feat, reg_param)
        new_cls.weight_method_tag = weight_method_tag
        return new_cls

    def forward(self, _x: torch.Tensor) -> torch.Tensor:
        """
        Follow S Rendle's paper to calculate interaction part in linear time.
        :param _x: feature tensor
        :return: prediction tensor of shape (M_sample, 1)
        """
        logger.debug(f"[{self.__repr__()}] calculating forward pass ...")
        x = _x.clone().detach()
        prediction = self.linear(x)
        trans_prediction = torch.sigmoid(prediction.squeeze())
        # trans_prediction = prediction.squeeze()
        return trans_prediction

    def loss(self, prediction, target):
        loss_weights = self.loss_weights(target)
        mse_loss = weighted_mse(prediction.squeeze(), target.squeeze(), loss_weights)
        linear_reg_loss = torch.linalg.norm(self.linear.weight) * self.reg_param
        return mse_loss + linear_reg_loss


class MLP(NNModule):

    def __init__(self, n_feat: int, dropout: float = 0.1):
        """
        # Initially we fill V with random values sampled from Gaussian distribution
        # NB: use nn.Parameter to compute gradients
        :param n_feat: number of features
        """
        super().__init__()
        self.n_feat = n_feat
        self.dropout = dropout
        self.layers = nn.Sequential(
            nn.Linear(n_feat, 128),
            nn.Tanh(),
            nn.Dropout(dropout),
            nn.Linear(128, 64),
            nn.Tanh(),
            nn.Dropout(dropout),
            nn.Linear(64, 1),
            nn.Sigmoid()
        )

    def __str__(self):
        return f"MLP model with {self.n_feat} features."

    def __repr__(self):
        return f"MLP(n_feat={self.n_feat})"

    @classmethod
    def weighted_loss_model(cls, n_feat: int, dropout: float = 0.1,
                            weight_method_tag: str = "no_weights", *args, **kwargs):
        new_cls = cls(n_feat, dropout)
        new_cls.weight_method_tag = weight_method_tag
        return new_cls

    def forward(self, _x: torch.Tensor) -> torch.Tensor:
        """
        Follow S Rendle's paper to calculate interaction part in linear time.
        :param _x: feature tensor
        :return: prediction tensor of shape (M_sample, 1)
        """
        logger.debug(f"[{self.__repr__()}] calculating forward pass ...")
        x = _x.clone().detach()
        prediction = self.layers(x).squeeze()

        return prediction

    def loss(self, prediction, target):
        loss_weights = self.loss_weights(target)
        mse_loss = weighted_mse(prediction.squeeze(), target.squeeze(), loss_weights)
        return mse_loss


class CNN(NNModule):

    def __init__(self, n_cat: int, n_feat: int, kernel_size: int = 3, dropout: float = 0.1):
        """
        The input tensor should have size of (N, C, L_in)
        """
        from math import ceil

        super().__init__()
        self.n_cat = n_cat
        self.n_feat = n_feat
        self.kernel_size = kernel_size
        self.dropout = dropout

        linear_start_size = ceil((n_feat - 2 * (kernel_size - 1)) / kernel_size) * n_cat

        self.layers = nn.Sequential(
            nn.Conv1d(self.n_cat, self.n_cat, kernel_size),
            nn.BatchNorm1d(self.n_cat),
            nn.ReLU(inplace=True),
            nn.Conv1d(self.n_cat, self.n_cat, kernel_size),
            nn.BatchNorm1d(self.n_cat),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size, ceil_mode=True),
            nn.Flatten(),
            nn.Linear(linear_start_size, 128),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(128, 64),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(64, self.n_cat),
            nn.Sigmoid()
        )

    def __str__(self):
        return f"CNN model with {self.n_cat} channels, n_feat={self.n_feat}"

    def __repr__(self):
        return f"CNN(n_cat={self.n_cat}; n_feat={self.n_feat})"

    @classmethod
    def weighted_loss_model(cls, n_cat: int, n_feat: int, kernel_size: int = 3, dropout: float = 0.1,
                            weight_method_tag: str = "no_weights", *args, **kwargs):
        new_cls = cls(n_cat, n_feat, kernel_size, dropout)
        new_cls.weight_method_tag = weight_method_tag
        return new_cls

    def forward(self, _x: torch.Tensor) -> torch.Tensor:
        """
        Follow S Rendle's paper to calculate interaction part in linear time.
        :param _x: feature tensor
        :return: prediction tensor of shape (M_sample, 1)
        """
        logger.debug(f"[{self.__repr__()}] calculating forward pass ...")
        x = _x.clone().detach()
        prediction = self.layers(x).squeeze()

        return prediction

    def loss(self, prediction, target):
        loss_weights = self.loss_weights(target) * 1000
        mse_loss = weighted_mse(prediction.squeeze(), target.squeeze(), loss_weights)
        return mse_loss


class GetModel:
    def __init__(self, model: nn.Module,
                 modelparam_str: str,
                 hyperparam_str: str,
                 train_loader: torch.utils.data.DataLoader,
                 val_loader: torch.utils.data.DataLoader,
                 optimizer: torch.optim.Optimizer,
                 criterion: Union[Callable, torch.nn.Module],
                 eval_metrics: Optional[Dict[str, Metric]],
                 logging_params: Optional[Dict[str, Any]] = None):
        self.model = model
        self.model_name = repr(self.model) + '_' + modelparam_str
        self.hyperparam_str = hyperparam_str
        self.logging_params = self._parse_logging_params(logging_params)
        self.writer = SummaryWriter(log_dir=self.logging_params["log_dir"]) if self.logging_params["log_dir"] else None
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.optimizer = optimizer
        self.criterion = criterion
        self.trainer = self.get_trainer()
        self.train_evaluator = create_supervised_evaluator(self.model, eval_metrics)
        self.val_evaluator = create_supervised_evaluator(self.model, eval_metrics)

    def __repr__(self):
        return self.model_name

    @staticmethod
    def _parse_logging_params(logging_params) -> Dict[str, Any]:
        default_logging_params = {
            "log_dir": os.path.join("runs", datetime.now().strftime("%Y_%m_%d"), f"{str(datetime.now())}"),
            "log_model_notes": None,
            "log_iter_interval": 500,
        }
        if isinstance(logging_params, dict):
            default_logging_params.update(logging_params)

        if default_logging_params["log_model_notes"]:
            default_logging_params["log_dir"] =  \
                default_logging_params["log_dir"] + "_" + default_logging_params["log_model_notes"]

        return default_logging_params

    def get_trainer(self) -> Engine:
        trainer = create_supervised_trainer(self.model, self.optimizer, self.criterion)
        log_iter_interval = self.logging_params["log_iter_interval"]

        @trainer.on(Events.ITERATION_COMPLETED(every=log_iter_interval))
        def log_training_loss(_trainer):
            # prep msg info
            curr_epoch = _trainer.state.epoch
            curr_iter = _trainer.state.iteration
            training_loss = _trainer.state.output
            msg = f"[class GetModel] Model[{self.model_name}]: Epoch[{curr_epoch}] Iteration[{curr_iter}] Loss: {training_loss:.2f}"

            logger.info(msg)
            if self.writer:
                self.writer.add_scalar("loss/train", training_loss, curr_iter)

        @trainer.on(Events.EPOCH_COMPLETED)
        def log_training_results(_trainer):
            self.train_evaluator.run(self.train_loader)
            # prep msg info
            curr_epoch = _trainer.state.epoch
            curr_iter = _trainer.state.iteration
            metrics = self.train_evaluator.state.metrics
            metrics_msg = ",".join(f"Avg {_m}: {_v:.4f}" for _m, _v in metrics.items())
            msg = f"[class GetModel] Model[{self.model_name}]: Epoch[{curr_epoch}] Iteration[{curr_iter}] Training results {metrics_msg}]"

            logger.info(msg)
            if self.writer:
                for _m, _v in metrics.items():
                    self.writer.add_scalar(f"{_m}/train", _v, curr_iter)

        @trainer.on(Events.EPOCH_COMPLETED)
        def log_validation_results(_trainer):
            self.val_evaluator.run(self.val_loader)
            # prep msg info
            curr_epoch = _trainer.state.epoch
            metrics = self.val_evaluator.state.metrics
            metrics_msg = ",".join(f"Avg {_m}: {_v:.4f}" for _m, _v in metrics.items())
            msg = f"[class GetModel] Model[{self.model_name}]: Epoch[{curr_epoch}] Validation results {metrics_msg}]"

            logger.info(msg)
            if self.writer:
                for _m, _v in metrics.items():
                    self.writer.add_scalar(f"{_m}/validation", _v, curr_epoch)

        return trainer

    def add_early_stopping_callback(self, monitor: str, mode: str = 'min',
                                    min_delta: float = 0.0,
                                    patience: int = 0) -> None:
        def _score_function(engine):
            val_loss = engine.state.metrics[monitor]
            logger.info(f"EarlyStopping Metrics: {val_loss:.4f}")
            return val_loss

        if mode == 'min':
            def f(x): return -1 * _score_function(x)
        elif mode == 'max':
            def f(x): return 1 * _score_function(x)
        else:
            logger.warning("[class GetModel.add_early_stopping_callback] mode can only be `min` or `max`,"
                           "use default `min` mode.")

            def f(x): return -1 * _score_function(x)

        early_stopping_handler = EarlyStopping(patience=patience, score_function=f, min_delta=min_delta,
                                               trainer=self.trainer)
        self.val_evaluator.add_event_handler(Events.COMPLETED, early_stopping_handler)
        return

    def add_best_checkpoint_callback(self, monitor, saving_path):

        to_save = {'model': self.model}
        score_function = Checkpoint.get_default_score_fn(monitor, -1.0)

        handler = Checkpoint(
            to_save, DiskSaver(saving_path, create_dir=True, require_empty=False),
            n_saved=1, filename_prefix="best",
            score_function=score_function, score_name=f"[{self.model_name}]_{self.hyperparam_str}_{monitor}",
            global_step_transform=global_step_from_engine(self.trainer)
        )

        self.val_evaluator.add_event_handler(Events.COMPLETED, handler)
        return

    def add_end_of_training_eval(self, fig_save_path: Optional[str] = None):
        @self.trainer.on(Events.COMPLETED)
        def _plot_mae_per_bin(engine):
            from itertools import islice
            # fetching y_true and y_pred
            y_true_holder = list()
            y_pred_holder = list()

            for X, y in islice(self.val_loader, 20):
                y_true_holder.append(y.flatten())
                y_pred_holder.append(self.model(X))

            y_true = torch.hstack(y_true_holder).squeeze().detach().numpy()
            y_pred = torch.hstack([i.flatten() for i in y_pred_holder]).squeeze().detach().numpy()
            y_pred_unflattened = torch.vstack(y_pred_holder).squeeze().detach().numpy()

            # mae_per_bin
            val, step, fig_mae = mae_per_bin(y_true, y_pred, 20)

            if self.writer:
                for i, pairs in enumerate(zip(val, step)):
                    self.writer.add_scalar("mae_per_bin", pairs[0], int(pairs[1] * 100))
                self.writer.add_figure('mae_per_bin_img', fig_mae)
            else:
                save_plot(fig_mae, savepath=fig_save_path,
                          figname=f"{self.model_name}_{self.hyperparam_str}_mae_per_bin")

            # y distro
            fig_distro = y_distribution(y_true, y_pred)
            if self.writer:
                self.writer.add_figure('y_distribution', fig_distro)
            else:
                save_plot(fig_distro, savepath=fig_save_path,
                        figname=f"{self.model_name}_{self.hyperparam_str}_y_distribution")

            # pred distro
            fig_pred_distro = pred_distribution_percat(y_pred_unflattened)
            if self.writer:
                self.writer.add_figure('pred_distribution_per_pcbk', fig_pred_distro)
            else:
                save_plot(fig_distro, savepath=fig_save_path,
                        figname=f"{self.model_name}_{self.hyperparam_str}_pred_distribution_per_pcbk")
            
            # rmse above
            rmse_above_threshold(y_true, y_pred, threshold=.5)

            return
