import numpy as np
from matplotlib.image import NonUniformImage
import matplotlib.pyplot as plt
import logging

logger = logging.getLogger(__name__)


def mae_hist_2d(y_true: np.ndarray, y_pred: np.ndarray):
    """
    y values should be within 0 ~ 1
    :param y_true: true y values
    :param y_pred: predicted y values
    :return: Nne
    """

    xedges = np.linspace(0, 1, 21)
    yedges = np.linspace(-1, 1, 41)

    mae = y_pred - y_true

    H, xedges, yedges = np.histogram2d(y_true, mae, bins=(xedges, yedges))
    H = H.T
    H = np.divide(H, np.sum(H, axis=0))  # normalize H
    fig = plt.figure(figsize=(5, 4))
    ax = fig.add_subplot(title='x-axis: y_true; y-axis: MAE',
                         aspect='auto', xlim=xedges[[0, -1]], ylim=yedges[[0, -1]])

    im = NonUniformImage(ax, interpolation='bilinear', cmap="Greens")
    xcenters = (xedges[:-1] + xedges[1:]) / 2
    ycenters = (yedges[:-1] + yedges[1:]) / 2
    im.set_data(xcenters, ycenters, H)

    avg_pct = 1 / len(yedges)
    im.set_clim(avg_pct * .1, avg_pct * 5)
    ax.images.append(im)

    return fig


def mae_per_bin(y_true: np.ndarray, y_pred: np.ndarray, n_bins: int = 10):

    _, bin_edges = np.histogram(y_true, bins=n_bins)

    y = np.expand_dims(y_true, axis=1)
    mae = np.expand_dims(np.abs(y_pred - y_true), axis=1)
    y = np.hstack((y, mae, np.zeros(y.shape)))

    # assign edges:
    for i in range(len(bin_edges) - 1):
        y[(y[:, 0] > bin_edges[i]) & (y[:, 0] <= bin_edges[i+1]), 2] = bin_edges[i+1]
    # sort by edges
    y = y[y[:, 2].argsort()]

    # group by edges
    val, idx = np.unique(y[:, 2], return_index=True)
    grp = np.split(y, idx[1:])
    mean = np.array([np.mean(arr) for arr in grp])

    fig = plt.figure(figsize=(5, 4))
    plt.plot(val, mean)
    plt.xlabel("y_true")
    plt.ylabel("Avg. prediction MAE")

    return mean, val, fig


def y_distribution(y_true: np.ndarray, y_pred: np.ndarray, n_bins: int = 20):
    def _norm_vals(arr):
        n_arr = np.divide(arr, np.sum(arr, axis=0))  # normalize H
        return n_arr
    true_val, true_edges = np.histogram(y_true, bins=n_bins)
    pred_val, pred_edges = np.histogram(y_pred, bins=n_bins)

    fig = plt.figure(figsize=(5, 4))
    plt.plot(true_edges[2:], _norm_vals(true_val[1:]), label="y_true distribution")
    plt.plot(pred_edges[2:], _norm_vals(pred_val[1:]), label="y_pred distribution")
    plt.ylabel("% of records")
    plt.xlabel("y values")
    plt.legend()

    return fig


def pred_distribution_percat(y_pred_unflattened: np.ndarray):
    
    fig = plt.figure(figsize=(5, 4))
    plt.boxplot(y_pred_unflattened, showfliers=False)
    plt.xlabel('prodcatbucket #')
    plt.ylabel('pred. distro')

    return fig


def rmse_above_threshold(y_true: np.ndarray, y_pred: np.ndarray, threshold: float = 0.5):
    _idx = y_true >= threshold
    _yt, _yp = y_true[_idx], y_pred[_idx]
    err = np.power(_yt - _yp, 2)
    rmse = (err.sum() / err.size) ** 0.5
    logger.info(f"[func rmse_above_threshold] Greater than {threshold:.2f} score rmse: {rmse:.4f}")

    return rmse
