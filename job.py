import os
import subprocess
from datetime import datetime

os.chdir("source")

# running a dataset ending first day of the month
dt = datetime.today().replace(day=1).strftime('%Y-%m-%d')

try:
  # updating to latest code
  subprocess.run(["git", "pull", "origin", "master"])
except:
  print("Unable to pull latest code.")

result = subprocess.run(["sh", "./refresh.sh", dt])

if result.returncode == 1:
  raise ValueError("The shell script exit with error(s).")
