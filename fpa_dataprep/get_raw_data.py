import databricks.koalas as ks
import pyspark
import pyspark.sql.functions as f
from pyspark.sql.types import FloatType, MapType, StringType
import pandas as pd
import yaml
import os
import re
import json
from io import StringIO
from collections import OrderedDict
from typing import Tuple
from sailthru.sailthru_client import SailthruClient
import fpa_dataprep.spark_utils as su
import fpa_dataprep.constant as C
import logging

logger = logging.getLogger(__name__)


def refresh_all_regions_data(spark, sc, dt):
    # first refresh email data
    lookback_window_mo = 6
    current_dt = dt.strftime("%Y-%m-%d")
    period_start_day_id = int((dt + pd.DateOffset(months=-lookback_window_mo)).strftime("%Y%m%d"))
    period_ending_day_id = int(dt.strftime("%Y%m%d"))

    refresh_raw_trans(spark, current_dt)
    refresh_email_recipient_gcid_mapping(spark)
    refresh_email_campaign(spark, sc, period_start_day_id, period_ending_day_id)
    refresh_email_activity(spark, period_start_day_id, period_ending_day_id)
    refresh_email_click_url_type(spark)

    return


def refresh_raw_trans(spark: pyspark.sql.SparkSession, CURRENT_DT: str):
    logger.info(f"[func refresh_raw_trans] Refreshing transactions feature ending on {CURRENT_DT}.")
    all_trans_query = '''
    SELECT p.sk_goldencustomerid
        , cal.daydate as trans_dt
        , cal.calendarmonthnum
        , case when cal.daydate < add_months('{CURRENT_DATE}', 0) then 'TRAIN' else 'TEST' end as timeframe
        , cal.sk_dayid as dayid
        , case when cal.sk_dayid between 20200313 and 20210912 then 1 else 0 end as covid_first6mo
        , tr.sk_locationid
        , tr.sk_originatinglocationid
        , l.channel
        , l.country
        , l.regions as region
        , l.businessunit
        , l.globalregiondesc
        , tr.sk_itemid
        , cast(greatest(tr.salesaftermarkdownamt, tr.grosssalesamt) as double) grosssalesamt  -- could be data error
        , cast(tr.salesaftermarkdownamt as double) salesaftermarkdownamt
        , cast(tr.discountsalesamt as double) discountsalesamt
        , cast(tr.salesunits as double) salesunits
        , case when salesaftermarkdownamt < grosssalesamt then 1 else 0 end as mkdntaken
        , case when discountsalesamt / salesaftermarkdownamt > 0.1 then 1 else 0 end as dscttaken
        , tr.ordernumber
        , ec.event
        , case when length(h.holiday1) = 0 then 'normal' else h.holiday1 end as holiday
    FROM cip_analytics.vw_factsalesdetail tr
    JOIN cip_analytics.vw_dimparty p
        ON tr.sk_partyid = p.sk_partyid
    JOIN cip_analytics.vw_dimgoldencustomer c
        ON p.sk_goldencustomerid = c.sk_goldencustomerid
    JOIN cip_analytics.vw_dimday cal
        ON nvl(tr.sk_orderdateid, tr.sk_dayid) = cal.sk_dayid
    JOIN {LOC_TBL} l  -- exclude invalid locations
        ON tr.sk_originatinglocationid = l.sk_locationid
    LEFT JOIN {EVENT_TBL} ec
        ON cal.sk_dayid = ec.sk_dayid
    LEFT JOIN {HOLIDAY_TBL} h
        ON cal.sk_dayid = h.sk_dayid
    WHERE sk_currencyversionid = 3
        AND salereturnvoidflag = 'S'
        AND c.rfeflag = 'N'
        AND l.channel <> 'RO'
        AND cal.daydate >= add_months('{CURRENT_DATE}', -12)
        AND cal.daydate < add_months('{CURRENT_DATE}', 6)
    '''.format(CURRENT_DATE=CURRENT_DT,
               LOC_TBL=C.DIM_LOC_TBL,
               EVENT_TBL=C.DIM_EVENT_TBL,
               HOLIDAY_TBL=C.DIM_HOLIDAY_TBL)

    su.save_table(spark.sql(all_trans_query), C.FEAT_ALL_TRANS_TBL)

    item_info_query = '''
    WITH allitems as (
    SELECT di.sk_itemid
        , case when lower(productcategorydesc) like 'sport%' then 'sport'
               when lower(productcategorydesc) = 'jewelry' then 'jewelry'
               when lower(productcategorydesc) = 'handbags' then 'handbags'
               when lower(productcategorydesc) = 'shoes' then 'shoes'
               when lower(productcategorydesc) = 'small leather goods' then 'slg'
               when lower(productcategorydesc) = 'ready to wear' then 'rtw'
               else 'oth' end as prodcat
    FROM cip_analytics.vw_dimitem di
    JOIN cip_analytics.vw_dimstylecolor dc
        ON di.sk_stylecolorid = dc.sk_stylecolorid
    WHERE material not in ('80269-960', '81264-961', '81264-962') --excluding mask
    )
    , itemtrans as (
    select t.region 
        , t.sk_itemid
        , ai.prodcat
        , sum(t.salesunits) as ttlunits
        , sum(t.salesaftermarkdownamt - t.discountsalesamt) as netsales
        , round(sum(t.grosssalesamt) / sum(t.salesunits), -1) as msrp
        , (sum(salesaftermarkdownamt) - sum(discountsalesamt)) / sum(grosssalesamt) as avg_fp_pct
        , concat(ai.prodcat, '_', case when ntile(5) over (partition by t.region, ai.prodcat order by sum(t.grosssalesamt) / sum(t.salesunits) desc) = 1 then 'high'
               when ntile(5) over (partition by t.region, ai.prodcat order by sum(t.grosssalesamt) / sum(t.salesunits) desc) = 5 then 'low'
               else 'mid' end) as prodcatbucket
    from {FEAT_ALL_TRANS_TBL} t
    join allitems ai
        on t.sk_itemid = ai.sk_itemid
    where t.trans_dt < add_months('{CURRENT_DATE}', 0)
    group by t.region, t.sk_itemid, ai.prodcat
    having sum(t.grosssalesamt) / sum(t.salesunits) > 1
    )
    select ip.region
        , ip.sk_itemid
        , ip.msrp
        , ip.prodcatbucket
        , ip.avg_fp_pct
        , ip.ttlunits
        , ip.netsales
        , percent_rank() over (order by ip.region, ip.ttlunits) as item_popularity
        , "mainline" as line
    from itemtrans ip
    '''.format(CURRENT_DATE=CURRENT_DT, FEAT_ALL_TRANS_TBL=C.FEAT_ALL_TRANS_TBL)

    su.save_table(spark.sql(item_info_query), C.FEAT_ALL_ITEMS_TBL)

    prodcataur_info_query = '''
        select region, prodcatbucket
            , sum(ttlunits) as prodcat_ttlunits
            , sum(ttlunits * avg_fp_pct) / sum(ttlunits) as prodcatbucket_avg_fp_pct_itmweighted
            , avg(avg_fp_pct) as prodcatbucket_avg_fp_pct_plain_avg
            , percent_rank() over (partition by region order by sum(ttlunits)) as prodcatbucket_popularity
            , row_number() over (partition by region order by prodcatbucket) as prodcatbucket_idx
        from {FEAT_ALL_ITEMS_TBL} ip
        group by region, prodcatbucket
        '''.format(FEAT_ALL_ITEMS_TBL=C.FEAT_ALL_ITEMS_TBL)
    su.save_table(spark.sql(prodcataur_info_query), C.FEAT_ALL_PRODCATAUR_TBL)

    # save prodcataur weights by net sales penentration, for future combination of scores
    prodcataur_weights = spark.sql('''
        select region, prodcatbucket
            , round(sum(netsales) / sum(sum(netsales)) over (partition by line), 4) as weights
        from {FEAT_ALL_ITEMS_TBL}
        group by line, region, prodcatbucket
        '''.format(FEAT_ALL_ITEMS_TBL=C.FEAT_ALL_ITEMS_TBL))
    su.save_table(prodcataur_weights, C.FEAT_ALL_PRODCATAUR_WEIGHTS_TBL)

    return


def get_prodcataur_info(REGION):
    # save prodcataur weights by net sales penentration, for future combination of scores
    prodcataur_info = ks.read_table(C.FEAT_ALL_PRODCATAUR_TBL) \
            .query(f"region = '{REGION}'") \
            .drop(columns=["region"])
    return prodcataur_info


def fetch_region_gcid_trans(spark: pyspark.sql.SparkSession, region: str, mode: str):

    if mode == "train":
        join_method = "inner"
    elif mode == "score":
        join_method = "left"
    else:
        raise ValueError("[func fetch_region_gcid_trans] mode can only be either `train` or `score`.")

    gcid_trans_query = '''
    with tmp as (
    SELECT tr.sk_goldencustomerid
      , tr.sk_itemid
      , tr.channel
      , tr.dayid
      , tr.covid_first6mo
      , tr.calendarmonthnum
      , tr.event
      , tr.holiday
      , tr.ordernumber
      , tr.timeframe
      , (salesaftermarkdownamt - discountsalesamt) / grosssalesamt as fp_pct
      , salesaftermarkdownamt - discountsalesamt as net_spend
      , mkdntaken
      , dscttaken
      , ip.prodcatbucket
      , ip.msrp
      , ip.avg_fp_pct as item_avg_fp_pct
      , ip.ttlunits as item_ttlunits
      , ip.item_popularity
    FROM {FEAT_ALL_TRANS_TBL} tr
    JOIN {FEAT_ALL_ITEMS_TBL} ip
        on tr.sk_itemid = ip.sk_itemid
        and tr.region = ip.region
    WHERE tr.region = "{REGION}"
    )
    , train_cust as (
    SELECT sk_goldencustomerid
    FROM tmp
    where timeframe = 'TRAIN'
    GROUP BY sk_goldencustomerid
    )
    , test_cust as (
    SELECT sk_goldencustomerid
    FROM tmp
    where timeframe = 'TEST'
    GROUP BY sk_goldencustomerid
    )
    , included_cust as (
    SELECT tr.sk_goldencustomerid
    FROM train_cust tr
    {join_method} JOIN test_cust t
        ON tr.sk_goldencustomerid = t.sk_goldencustomerid
    )
    SELECT tmp.*
    FROM tmp
    JOIN included_cust
        ON tmp.sk_goldencustomerid = included_cust.sk_goldencustomerid
    '''.format(join_method=join_method, REGION=region,
               FEAT_ALL_TRANS_TBL=C.FEAT_ALL_TRANS_TBL,
               FEAT_ALL_ITEMS_TBL=C.FEAT_ALL_ITEMS_TBL)

    su.save_table(spark.sql(gcid_trans_query), C.FEAT_REGION_GCID_TBL.format(region=region))
    kdf_gcid_trans = ks.read_table(C.FEAT_REGION_GCID_TBL.format(region=region))
    n_cust = kdf_gcid_trans["sk_goldencustomerid"].nunique()
    logger.info(f"[func fetch_region_gcid_trans] Total {region} customers: {n_cust}.")
    return kdf_gcid_trans


def _fetch_email_campaign_cip(spark, period_start_day_id: int, period_ending_day_id: int):
    """
    day_id is an int like 20210801
    """
    from fpa_dataprep.data_transform_utils import parsing_email_campaign

    with StringIO(C.EMAIL_NOMENCLATURE_YML_STRING) as file:
        nomen = OrderedDict(yaml.safe_load(file))
    
    su.refresh_table(spark, "sailthru.dimsttemplate")

    raw_data = spark.sql('''
    select sk_campaignid, sk_sentdayid, campaignname, t.templatename, blastid
    from cip_analytics.vw_dimstcampaign c
    left join cip_analytics.vw_dimsttemplate t
        on c.sk_templateid = t.sk_templateid
    where sk_sentdayid between {st} and {et}
        and c.sailthruaccount in (7533)  -- tb mainline prod, 7545 for JP
    '''.format(st=period_start_day_id, et=period_ending_day_id))

    parsing_func = f.udf(lambda row: parsing_email_campaign(nomen, row), MapType(StringType(), StringType()))
    _df = raw_data.withColumn('test', parsing_func("campaignname"))
    final = su.exploding_map_to_columns(_df, nomen.keys(), 'test').drop("test")

    return final


def _pred_email_type_by_subject(df, subject_col):
    import fpa_dataprep
    import joblib

    modelfile = os.path.join(os.path.dirname(fpa_dataprep.__file__), "email_subject.joblib")
    pipe = joblib.load(modelfile)["model"]

    def cleanup(subject):
        return " ".join(re.sub("[^\%\s\w]", "", subject, re.UNICODE).split())

    mapping = {
        0: "off-price",
        1: "full-price",
        2: "na"
    }

    X = df[subject_col].apply(cleanup)
    logger.info(f"[func _pred_email_type_by_subject] predicting email types by subjects ...")
    pred = pipe.predict(X)
    result = list(map(lambda i: mapping[i], pred))

    return result


def get_sailthru_data_to_spkdf(spark, sc, endpoint, post_body, interested_field=None):
    api_key = os.environ["ST_API_KEY"]
    api_secret = os.environ["ST_API_SECRET"]
    stc = SailthruClient(api_key, api_secret, request_timeout=60)

    response = stc.api_get(endpoint, post_body)
    if response.is_ok():
        logger.info(f"[func get_sailthru_data_to_spkdf] Fetched {endpoint} data from sailthru API")
        _json = response.get_body()
        if interested_field:
            jrdd = sc.parallelize([json.dumps(_json[interested_field])])
        else:
            jrdd = sc.parallelize([json.dumps(_json)])
        _df = spark.read.json(jrdd)
        return _df


@f.udf(StringType())
def unpack_dynamic_subject(sub: str):
    if sub:
        regex_expression = "^(?P<pre_msg>.*)\{{1}(?P<condition>.+)\?{1}(?P<true_statement>.+):{1}(?P<false_statement>.+)\}{1}(?P<post_msg>.*)$"
        mobj = re.search(regex_expression, sub)
        if mobj:
            pre_msg = mobj["pre_msg"] if mobj["pre_msg"].lower() not in ("null", "nan") else ""
            false_statment = mobj["false_statement"].replace('\"', '').strip() 
            false_statment = false_statment if false_statment.lower() not in ("null", "nan") else ""
            post_msg = mobj["post_msg"] if mobj["post_msg"].lower() not in ("null", "nan") else ""
            return  pre_msg + false_statment + post_msg
        else:
            return sub


def refresh_email_campaign(spark, sc, start_day_id, ending_day_id):
    df = _fetch_email_campaign_cip(spark, start_day_id, ending_day_id)
    
    st_template_df = get_sailthru_data_to_spkdf(spark, sc, "template", {}, "templates")
    st_blast_df = get_sailthru_data_to_spkdf(spark, sc, "blast", {
        "status": "sent", 
        "limit": 0, 
        "start_date": str(start_day_id), 
        "end_date": str(ending_day_id)
        }, "blasts")
    
    final_df = df.join(st_template_df.selectExpr("name as templatename", "labels as labels_t", "subject as subject_t"), on="templatename", how="left") \
        .join(st_blast_df.selectExpr("blast_id as blastid", "labels as labels_b", "subject as subject_b"), on="blastid", how="left") \
        .withColumn("label", f.concat_ws("|", f.coalesce("labels_b", "labels_t"))) \
        .withColumn("subject", f.coalesce("subject_b", "subject_t")) \
        .withColumn("subject", unpack_dynamic_subject(f.col("subject"))) \
        .select(df["*"], "label", "subject")
    
    final_df.createOrReplaceTempView("tmp")
    final_df_processed = spark.sql('''
    select sk_campaignid
        , sk_sentdayid
        , campaignname
        , templatename
        , blastid
        , region
        , channel
        , type
        , type_of_trigger
        , case when offer = 'None' 
            and (subject rlike '%{1}'
                    or subject like '%$%'
                    or subject like '%€%'
                    or subject like '%off%'
                    or lower(templatename) like '%offer%'
                    or lower(templatename) like '%sale%'
                    or main_message in ('EventBounceback', 'Reactivation')
                    or lower(campaignname) like '%birthday%'
                    or lower(campaignname) like '%giftcard%'
                    or lower(campaignname) like '%promo%') then 'OthPromo'
                else offer end as offer 
        , main_message
        , category
        , story
        , secondary_message
        , segment
        , label
        , subject
    from tmp
    ''').toPandas()

    final_df_processed["pred_eml_typ"] = _pred_email_type_by_subject(final_df_processed, "subject")
    final_df_processed_spk = spark.createDataFrame(final_df_processed)

    su.save_table(final_df_processed_spk, C.DIM_EMAIL_CAMPAIGN_TBL)

    return


def refresh_email_recipient_gcid_mapping(spark):
    su.save_table(spark.sql('''
    select r.sk_profileid, r.emailaddress, r.sk_partyid
    , r.emailaddress rlike "\@{1}[0-9a-z-]+\.{1}[0-9a-z.-]+$" as valid_email
    , row_number() over (partition by r.sk_profileid order by lastopen desc) as rnk
    from cip_analytics.vw_dimstrecipient r
    where r.emailaddress is not null
    '''), C.DIM_EMAIL_RECIPIENT_TMP_TBL)

    su.save_table(spark.sql('''
    select sk_profileid, emailaddress, coalesce(sk_partyid, -1) as sk_partyid
    from {DIM_EMAIL_RECIPIENT_TMP_TBL}
    where rnk = 1 and valid_email = true
    '''.format(DIM_EMAIL_RECIPIENT_TMP_TBL=C.DIM_EMAIL_RECIPIENT_TMP_TBL)),
                  C.DIM_EMAIL_RECIPIENT_TBL)

    su.save_table(spark.sql('''
    with stknown as (
    select r.sk_profileid, r.sk_partyid, p.sk_goldencustomerid, r.emailaddress
    from {DIM_EMAIL_RECIPIENT_TBL} r
    left join cip_analytics.vw_dimparty p
    on p.sk_partyid = r.sk_partyid
    where r.sk_partyid <> -1
    )
    , stunknown as (
    select r.sk_profileid
    , max(r.sk_partyid) as sk_partyid
    , max(gc.sk_goldencustomerid) as sk_goldencustomerid
    , max(r.emailaddress) as emailaddress
    from {DIM_EMAIL_RECIPIENT_TBL} r
    left join cip_analytics.vw_dimgoldencustomer gc
    on r.emailaddress = gc.emailaddress
    where r.sk_partyid = -1
    group by r.sk_profileid
    )
    , combined as (
    select * from stknown
    union all
    select * from stunknown
    )
    select *
    from combined
    where sk_goldencustomerid is not null and sk_goldencustomerid <> -1
    '''.format(DIM_EMAIL_RECIPIENT_TBL=C.DIM_EMAIL_RECIPIENT_TBL))
                  , C.DIM_EMAIL_RECIPIENT_MATCHED_TBL)
    
    return


def refresh_email_activity(spark, start_day_id, ending_day_id):
    su.save_table(spark.sql('''
    select f.sk_profileid
    , m.sk_partyid
    , m.sk_goldencustomerid
    , m.emailaddress
    , f.sk_campaignid
    , f.activitytype
    , f.sk_activitydayid
    , f.activitytimestamp
    , f.engagementdesc
    , f.clickurl
    , f.device
    , row_number() over (partition by f.sk_profileid, f.sk_campaignid, f.activitytype order by f.activitytimestamp asc) as rnk
    from cip_analytics.vw_factstactivity f
    join {DIM_EMAIL_RECIPIENT_MATCHED_TBL} m
    on f.sk_profileid = m.sk_profileid
    where sk_accountid in (7533)
    and f.sk_activitydayid between {st} and {et}
    '''.format(st=start_day_id, et=ending_day_id,
               DIM_EMAIL_RECIPIENT_MATCHED_TBL=C.DIM_EMAIL_RECIPIENT_MATCHED_TBL)),
                  C.FACT_EMAIL_ACTIVITY_MATCHED_TBL)

    activity_query = '''
    select s.sk_profileid
    , s.sk_partyid
    , s.sk_goldencustomerid
    , s.emailaddress
    , cam.*
    , s.activitytype
    , s.sk_activitydayid
    , s.activitytimestamp
    , s.engagementdesc
    , case when o.sk_profileid is not null then 1 else 0 end as opened
    , case when c.sk_profileid is not null then 1 else 0 end as clicked
    , c.clickurl
    from (select * from {FACT_EMAIL_ACTIVITY_MATCHED_TBL} where activitytype = 'SEND' and rnk = 1) as s
    left join (select * from {FACT_EMAIL_ACTIVITY_MATCHED_TBL} where activitytype = 'OPEN' and rnk = 1) as o
    on s.sk_profileid = o.sk_profileid and s.sk_campaignid = o.sk_campaignid
    left join (select * from {FACT_EMAIL_ACTIVITY_MATCHED_TBL} where activitytype = 'CLICK' and rnk = 1) as c
    on s.sk_profileid = c.sk_profileid and s.sk_campaignid = c.sk_campaignid
    join {DIM_EMAIL_CAMPAIGN_TBL} cam
    on s.sk_campaignid = cam.sk_campaignid
    '''.format(st=start_day_id, et=ending_day_id,
               DIM_EMAIL_CAMPAIGN_TBL=C.DIM_EMAIL_CAMPAIGN_TBL,
               FACT_EMAIL_ACTIVITY_MATCHED_TBL=C.FACT_EMAIL_ACTIVITY_MATCHED_TBL)
    su.save_table(spark.sql(activity_query), C.FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL)

    return


def refresh_email_click_url_type(spark):
    from fpa_dataprep.data_transform_utils import parsing_url_type

    raw = spark.sql('''
    select clickurl, count(*) as clicks
    from {FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL}
    where clickurl is not null
    group by clickurl
    '''.format(FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL=C.FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL))

    with StringIO(C.ECOM_URL_TYPES_YML_STRING) as file:
        word_matching = OrderedDict(yaml.safe_load(file))
    parsing_url_func = f.udf(lambda row: parsing_url_type(word_matching, row), StringType())
    raw_typed = raw.withColumn("clickurl_type", parsing_url_func("clickurl"))

    su.save_table(raw_typed, C.FEAT_EMAIL_CLICKURLTYPE_TBL)

    return


# noinspection PyRedundantParentheses
def fetch_email_gcid_level_data(spark, region):
    from fpa_dataprep.data_transform_utils import two_sample_proportion_ztest
    # JP emails are not available currently
    if region == "JP":
        logger.info("[func fetch_email_gcid_level_data] JP data not available, return None.")
        return
        
    region_mapping = {
        "NA": ("'US'", "'CA'"),
        "EU": ("'EU'", "'UK'", "'DE'", "'FR'", "'IT'"),
        "AS": ("'HK'", "'SG'", "'MO'", "'AU'", "'CN'"),
        "JP": ("'JP'")
    }

    if region != "AS":
        # for NA/EU, use naming convention
        gcid_df = spark.sql("""
        with gclevel as (
        select sk_goldencustomerid
        , case when offer in ("MD", "PS", "TargetedPromo", "Event", "OthPromo") then 'off-price'
                when offer = 'FP' then 'full-price'
                else 'na' end as msgtyp
        , nullif(count(sk_campaignid), 0) as received
        , sum(opened) as opened
        , sum(clicked) as clicked
        from {FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL}
        where region in ({countries})
        group by sk_goldencustomerid
        , case when offer in ("MD", "PS", "TargetedPromo", "Event", "OthPromo") then 'off-price'
                when offer = 'FP' then 'full-price'
                else 'na' end
        )
        select *
        from gclevel
        """.format(countries=", ".join(region_mapping[region]),
                FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL=C.FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL))
    else:
        # for APAC, use predicted value instead of naming convention
        logger.warning("[func fetch_email_gcid_level_data] APAC email types are predicted based on subjects.")
        gcid_df = spark.sql("""
        with gclevel as (
        select sk_goldencustomerid
        , pred_eml_typ as msgtyp
        , nullif(count(sk_campaignid), 0) as received
        , sum(opened) as opened
        , sum(clicked) as clicked
        from {FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL}
        where region in ({countries})
        group by sk_goldencustomerid
        , pred_eml_typ
        )
        select *
        from gclevel
        """.format(countries=", ".join(region_mapping[region]),
                FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL=C.FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL))

    gcid_clicks = spark.sql('''
    select sk_goldencustomerid, clickurl_type, count(1) as clicks
    from {FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL} cam
    left join {FEAT_EMAIL_CLICKURLTYPE_TBL} u
        on cam.clickurl = u.clickurl
    where region in ({countries})
    group by sk_goldencustomerid, clickurl_type
    '''.format(countries=", ".join(region_mapping[region]),
               FEAT_EMAIL_CLICKURLTYPE_TBL=C.FEAT_EMAIL_CLICKURLTYPE_TBL,
               FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL=C.FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL))

    pv_gcdf = gcid_df.groupBy("sk_goldencustomerid").pivot("msgtyp").agg(
        f.first("received").alias("received"),
        f.first("opened").alias("opened"),
        f.first("clicked").alias("clicked")
    )

    click_type_interested = ["sales", "new_arrivals", "product", "hompage", "sport", "social_media", "info"]
    pv_gcclickdf = gcid_clicks.groupby("sk_goldencustomerid").pivot("clickurl_type", click_type_interested).agg(
        f.first("clicks").alias("clicks")
    ).fillna(0)

    final_kdf = pv_gcdf.withColumn("pval_open", two_sample_proportion_ztest("full-price_opened", "full-price_received", "off-price_opened", "off-price_received")) \
        .withColumn("pval_ctr", two_sample_proportion_ztest("full-price_clicked", "full-price_received", "off-price_clicked", "off-price_received")) \
        .withColumn("fp_openrate", f.col("full-price_opened") / f.col("full-price_received")) \
        .withColumn("fp_ctr", f.col("full-price_clicked") / f.col("full-price_received")) \
        .withColumn("offp_openrate", f.col("off-price_opened") / f.col("off-price_received")) \
        .withColumn("offp_ctr", f.col("off-price_clicked") / f.col("off-price_received")) \
        .fillna(0.5, subset=["pval_open", "pval_ctr"]) \
        .fillna(0.0, subset=["fp_openrate", "fp_ctr", "offp_openrate", "offp_ctr"]) \
        .select(["sk_goldencustomerid", "fp_openrate", "fp_ctr", "offp_openrate", "offp_ctr", "pval_open", "pval_ctr"]) \
        .join(pv_gcclickdf, on="sk_goldencustomerid", how="left").fillna(0)

    su.save_table(final_kdf, C.FEAT_EMAIL_REGION_GCID_TBL.format(region=region))
    final_kdf = ks.read_table(C.FEAT_EMAIL_REGION_GCID_TBL.format(region=region))
    
    return final_kdf
