from pyspark.sql.types import *


ECOM_URL_TYPES_YML_STRING = """
sales:
    - sconti
    - sale
    - セール
    - soldes
new_arrivals:
    - novita
    - neu
    - new-arrivals
    - 新着
    - nouveautes
    - new
    - runway
sport:
    - sport
product:
    - 財布＆小物
    - scarpe
    - バッグ
    - accessories
    - maison
    - chaussures
    - シューズ
    - ウェア
    - accessori
    - abbigliamento
    - kleidung
    - home
    - accessoires
    - schuhe
    - borse
    - shoes
    - handbags
    - taschen
    - clothing
    - sacs
    - arredamento-casa
    - homeware
    - sqq
    - watches-jewelry
    - swim
    - mask
    - beauty-fragrance
    - \/[0-9]{0,10}\.html$
gifts:
    - gift
homepage:
    - ^http[s]?:\/\/www\.toryburch\.[a-zA-Z.]+\/?[a-z\-]{0,5}\/?$
    - ^http[s]?:\/\/toryburch\.[a-zA-Z.]+\/?[a-z\-]{0,5}\/?$
info:
    - store
    - client-services
    - clientservices
    - account
    - foundation
    - blog
    - empoweringwomen
    - customer
social_media:
    - facebook
    - instagram
    - youtube
    - tumblr
    - twitter
    - pinterest
    - linkedin
"""

EMAIL_NOMENCLATURE_YML_STRING = """
region:
    - US
    - TG
    - EU
    - UK
    - DE
    - FR
    - IT
    - HK
    - SG
    - MO
    - AU
    - CN
    - JP
    - CA
channel:
    - Ecom
    - Outlet
    - Retail
    - Fdn
    - Sport
    - OutletLOY
type:
    - Blast
    - Targeted
    - Trigger
type_of_trigger:
    - Marketing
    - Transactional
offer:
    - FP
    - NS
    - MD
    - PS
    - Event
    - TargetedPromo
main_message:
    - Inspo
    - NA
    - InColor
    - Neut
    - Best
    - W2W
    - PartEvt
    - Guide
    - Edit
    - Shop
    - SandalShp
    - BootShp
    - Sptlt
    - Class
    - TFaves
    - Getawy
    - Home
    - Launch
    - Fam
    - GG
    - Excl
    - Print
    - Shpsund
    - Serv
    - SptML
    - Trend
    - Redep
    - FDN
    - Essen
    - Wishlst
    - Cozy
    - Product
    - OnModel
    - Graphic
    - TextOnly
    - Other
    - Abandon
    - AuthorizationFailure
    - FoundationDonationConfirmation
    - OrderCancellation
    - OrderConfirmation
    - OrderConfirmationWardrobe
    - PasswordReset
    - PostPurchase
    - RefundDetails
    - ReturnOrderConfirmation
    - ShipConfirmation
    - Welcome
    - Wishlist
    - Reactivation
    - BackOrder
    - OrderDelay
    - SettlementFailure
    - Birthday
    - RatingsReviews
    - FraudHold
    - ClientTelling
    - EventBounceback
    - FragranceReplenishment
    - AdvancedExchange
    - Exchange
    - Wishlist
    - FoundationTaxReceipt
category:
    - CrossCat
    - Handbags
    - Shoes
    - RTW
    - Jewelry
    - Eyewear
    - Masks
    - Watches
    - Home
    - Swim 
    - Bty/Frgn
    - Nonsell
    - Survey
    - Running
    - Studio
    - Tennis
    - Golf
    - Cmgng
    - Legg
    - Lounge
    - Outrwr
    - Miller
    - Kira
    - Minnie
    - Walker
    - Robinson
    - Lee
    - Ella
    - Eleanor
    - McGraw
    - Perry 
    - Fleming
    - Gemini
    - Chevron
    - Seamless
    - TieDye
    - Sneaker
    - Melange
    - Track
    - Tees
    - Store
    - Other
story:
    - Miller
    - Kira
    - Minnie
    - Walker
    - Robinson
    - Lee
    - Ella
    - Eleanor
    - McGraw
    - Perry 
    - Fleming
    - Gemini
    - TMonogram
    - Boots
    - Chevron
    - Seamless
    - TieDye
    - Sneaker
    - Melange
    - Lounge
    - Track
    - Essen
    - Sport
    - Store
    - Tees
    - CS
    - FDN
    - Social
    - ToryDaily
    - Capsule
    - ModularMinor
    - DynamProd
    - GiftShop
    - Home
    - Masks
    - GiftCards
    - MD
    - Services
    - GiftGuide
    - Accs
    - PS
    - GWP
    - StoreEvent
    - Other
secondary_message:
    - Miller
    - Kira
    - Minnie
    - Walker
    - Robinson
    - Lee
    - Ella
    - Eleanor
    - McGraw
    - Perry 
    - Fleming
    - Chevron
    - Gemini
    - TMonogram
    - Seamless
    - TieDye
    - Sneaker
    - Melange
    - Track
    - Sport
    - Essen
    - Store
    - CS
    - FDN
    - Social
    - ToryDaily
    - Capsule
    - ModularMinor
    - DynamProd
    - GiftShop
    - Home
    - Masks
    - GiftCards
    - MD
    - Services
    - GiftGuide
    - Accs
    - PS
    - GWP
    - StoreEvent
    - Shop
    - Other
segment:
    - NonConverters
    - Customer
    - NewToFile
    - Balance
    - FullFile
    - Sport
    - Foundation
    - ShoeAffinity
    - HandbagAffinity
    - RTWAffinity
    - HomeAffinity
    - CRM
    - NonOpeners
    - Other
    - OutletLOY
    - EarlyAccess
"""

SEASONALITY_ADJ_CSV_STRING = """
region,month,prodcat,adjusted_param
NA,1,handbags,1.0659193572464176
NA,2,handbags,1.0613836672595258
NA,3,handbags,1.0462042673129428
NA,4,handbags,1.0243049832999491
NA,5,handbags,1.0013467094559485
NA,6,handbags,0.9832640854696719
NA,7,handbags,0.9747314112782391
NA,8,handbags,0.9779543556201435
NA,9,handbags,0.9920997977853524
NA,10,handbags,1.0135111866298765
NA,11,handbags,1.0366537472446342
NA,12,handbags,1.0555452024054568
NA,1,jewelry,0.96697987879245
NA,2,jewelry,0.9442435070106999
NA,3,jewelry,0.9332115926801992
NA,4,jewelry,0.9358586535886042
NA,5,jewelry,0.951710912593338
NA,6,jewelry,0.9779310953608931
NA,7,jewelry,1.0098262523524013
NA,8,jewelry,1.0416877138906775
NA,9,jewelry,1.0678128411894383
NA,10,jewelry,1.0835256979595862
NA,11,jewelry,1.0860139604050798
NA,12,jewelry,1.0748322735030469
NA,1,oth,1.0157858762081844
NA,2,oth,1.0029648301866507
NA,3,oth,0.9752490125013292
NA,4,oth,0.9413405804968228
NA,5,oth,0.9118860362522764
NA,6,oth,0.8961334590211978
NA,7,oth,0.8990288116903487
NA,8,oth,0.9196630171839184
NA,9,oth,0.9515573886767658
NA,10,oth,0.9846977948057388
NA,11,oth,1.0086788767294452
NA,12,oth,1.0159711020139777
NA,1,rtw,0.9946510761830251
NA,2,rtw,1.014836229321707
NA,3,rtw,1.0204998346825311
NA,4,rtw,1.0096147366728228
NA,5,rtw,0.9860770025326868
NA,6,rtw,0.9583114158339302
NA,7,rtw,0.9362560214745264
NA,8,rtw,0.9278050340361578
NA,9,rtw,0.935983287587916
NA,10,rtw,0.9578635668014812
NA,11,rtw,0.9856143354819296
NA,12,rtw,1.0093028524957215
NA,1,shoes,1.0653860514911973
NA,2,shoes,1.0829110677157032
NA,3,shoes,1.085251298590003
NA,4,shoes,1.0717606901877137
NA,5,shoes,1.0461635163073355
NA,6,shoes,1.0155262398655591
NA,7,shoes,0.9883067155793128
NA,8,shoes,0.9720192792060354
NA,9,shoes,0.971160308575466
NA,10,shoes,0.9859669347137016
NA,11,shoes,1.012351578460008
NA,12,shoes,1.0430303846718345
NA,1,slg,1.0164500418576643
NA,2,slg,0.9889474479523332
NA,3,slg,0.965125470221894
NA,4,slg,0.9475485210768344
NA,5,slg,0.9381087417590012
NA,6,slg,0.9378223152028612
NA,7,slg,0.9467200749447215
NA,8,slg,0.9638441859229524
NA,9,slg,0.987351254477798
NA,10,slg,1.0147107678609726
NA,11,slg,1.0429775014375344
NA,12,slg,1.0691085692133842
NA,1,sport,0.9886922191841544
NA,2,sport,0.9905057434343671
NA,3,sport,0.9776475044734224
NA,4,sport,0.9540912825901252
NA,5,sport,0.9271170210553136
NA,6,sport,0.9050609927534536
NA,7,sport,0.8947395129499144
NA,8,sport,0.8993423880496929
NA,9,sport,0.9174471204309232
NA,10,sport,0.9434585248672966
NA,11,sport,0.9693378950828334
NA,12,sport,0.9870873293771883
EU,1,handbags,1.1223070345799446
EU,2,handbags,1.1454367680968354
EU,3,handbags,1.1629978260228009
EU,4,handbags,1.1732182729748215
EU,5,handbags,1.1750668515160774
EU,6,handbags,1.1683570374781866
EU,7,handbags,1.15376586051149
EU,8,handbags,1.1327655908434533
EU,9,handbags,1.1074751851471107
EU,10,handbags,1.0804464808398446
EU,11,handbags,1.0544067121077243
EU,12,handbags,1.0319833281498043
EU,1,jewelry,1.0780377450715182
EU,2,jewelry,1.0912721692403329
EU,3,jewelry,1.1031020673960439
EU,4,jewelry,1.11009650419552
EU,5,jewelry,1.1102269363575965
EU,6,jewelry,1.1034555356348228
EU,7,jewelry,1.0917461598522182
EU,8,jewelry,1.0784947901661512
EU,9,jewelry,1.0675446204910923
EU,10,jewelry,1.0620714452131763
EU,11,jewelry,1.0636626077104263
EU,12,jewelry,1.07185663523163
EU,1,oth,1.111165517545763
EU,2,oth,1.0965337972752491
EU,3,oth,1.0605594251235717
EU,4,oth,1.0154530911982191
EU,5,oth,0.9765251242833904
EU,6,oth,0.956988745683056
EU,7,oth,0.9634751389493692
EU,8,oth,0.993782644029548
EU,9,oth,1.0376240611663647
EU,10,oth,1.0801184090803224
EU,11,oth,1.1068419387895254
EU,12,oth,1.1087239502277852
EU,1,rtw,1.0
EU,2,rtw,1.0
EU,3,rtw,1.0
EU,4,rtw,1.0
EU,5,rtw,1.0
EU,6,rtw,1.0
EU,7,rtw,1.0
EU,8,rtw,1.0
EU,9,rtw,1.0
EU,10,rtw,1.0
EU,11,rtw,1.0
EU,12,rtw,1.0
EU,1,shoes,0.980202422694324
EU,2,shoes,0.9940688844388844
EU,3,shoes,1.0098955365173818
EU,4,shoes,1.0224517861175226
EU,5,shoes,1.0275878845380586
EU,6,shoes,1.0236063887980962
EU,7,shoes,1.0118231541332423
EU,8,shoes,0.9961324534597064
EU,9,shoes,0.9817199486443046
EU,10,shoes,0.9733488671669036
EU,11,shoes,0.9737857902595698
EU,12,shoes,0.9828863180342732
EU,1,slg,1.1470318462253324
EU,2,slg,1.1782308199018143
EU,3,slg,1.2039263103825486
EU,4,slg,1.2218028956570612
EU,5,slg,1.230249715677837
EU,6,slg,1.2285056270446642
EU,7,slg,1.2167277896768092
EU,8,slg,1.195977505124354
EU,9,slg,1.1681245826292108
EU,10,slg,1.135678850515133
EU,11,slg,1.1015639954243044
EU,12,slg,1.0688541087588972
EU,1,sport,1.0
EU,2,sport,1.0
EU,3,sport,1.0
EU,4,sport,1.0
EU,5,sport,1.0
EU,6,sport,1.0
EU,7,sport,1.0
EU,8,sport,1.0
EU,9,sport,1.0
EU,10,sport,1.0
EU,11,sport,1.0
EU,12,sport,1.0
AS,1,handbags,1.0386558767945433
AS,2,handbags,1.0557111765753655
AS,3,handbags,1.0614324617443172
AS,4,handbags,1.054190729544436
AS,5,handbags,1.0360478946278362
AS,6,handbags,1.0121697068611095
AS,7,handbags,0.9893549243125206
AS,8,handbags,0.9740995251711696
AS,9,handbags,0.9707471291878564
AS,10,handbags,0.9802522530730712
AS,11,handbags,0.9999085340853956
AS,12,handbags,1.0241193038163998
AS,1,jewelry,1.0169651224040324
AS,2,jewelry,1.0436558049954667
AS,3,jewelry,1.0500877613991193
AS,4,jewelry,1.0348654839050562
AS,5,jewelry,1.0012916698999783
AS,6,jewelry,0.95665065300738
AS,7,jewelry,0.9106279595808816
AS,8,jewelry,0.8732088910556941
AS,9,jewelry,0.8525120656792594
AS,10,jewelry,0.8530279646631063
AS,11,jewelry,0.8746446561214563
AS,12,jewelry,0.9126720803430429
AS,1,oth,1.0259020218343116
AS,2,oth,1.0453937842224252
AS,3,oth,1.0454924862917072
AS,4,oth,1.0261740201644391
AS,5,oth,0.9921569010904994
AS,6,oth,0.9517497750074001
AS,7,oth,0.9148220404155596
AS,8,oth,0.8903932580641151
AS,9,oth,0.8844301326231927
AS,10,oth,0.8983891512768454
AS,11,oth,0.9288608382438864
AS,12,oth,0.9684025156897368
AS,1,rtw,0.9938252827437226
AS,2,rtw,1.0186272884378658
AS,3,rtw,1.031099886969369
AS,4,rtw,1.0276831349911577
AS,5,rtw,1.00935224576314
AS,6,rtw,0.9813392426674864
AS,7,rtw,0.951639629107122
AS,8,rtw,0.9287303027740444
AS,9,rtw,0.9191500698087228
AS,10,rtw,0.9256333312681766
AS,11,rtw,0.9463296272469072
AS,12,rtw,0.9753317972608766
AS,1,shoes,1.0056319939379388
AS,2,shoes,1.0309270174388003
AS,3,shoes,1.0445424938970724
AS,4,shoes,1.041784245827408
AS,5,shoes,1.023603228271836
AS,6,shoes,0.9962676701485014
AS,7,shoes,0.9692019909322
AS,8,shoes,0.9517375645481974
AS,9,shoes,0.9498955623728711
AS,10,shoes,0.9643110473925128
AS,11,shoes,0.9900140249523672
AS,12,shoes,1.0181429367413737
AS,1,slg,1.0238143873389682
AS,2,slg,1.0382510192828032
AS,3,slg,1.0414022273183554
AS,4,slg,1.03220987964369
AS,5,slg,1.0137606386750189
AS,6,slg,0.992249502765436
AS,7,slg,0.9748996119757898
AS,8,slg,0.9675368181690784
AS,9,slg,0.97263344503516
AS,10,slg,0.9884781162038452
AS,11,slg,1.0097504116122158
AS,12,slg,1.0293073905157086
AS,1,sport,0.9259695698676206
AS,2,sport,0.8966825325125791
AS,3,sport,0.8687600322760994
AS,4,sport,0.8467357314100767
AS,5,sport,0.8341856256389273
AS,6,sport,0.8331474243479163
AS,7,sport,0.8437896960378252
AS,8,sport,0.8643844985463714
AS,9,sport,0.8915879379554796
AS,10,sport,0.9209831029811932
AS,11,sport,0.9477972208081616
AS,12,sport,0.9676765927066706
JP,1,handbags,0.8572164870119195
JP,2,handbags,0.8267723413179181
JP,3,handbags,0.814317345573019
JP,4,handbags,0.8208855406600591
JP,5,handbags,0.8459316207125768
JP,6,handbags,0.8873762055959211
JP,7,handbags,0.941778475576442
JP,8,handbags,1.0046218356724013
JP,9,handbags,1.0706888932098058
JP,10,handbags,1.134494617129501
JP,11,handbags,1.1907417172161143
JP,12,handbags,1.2347604366731106
JP,1,jewelry,0.998998933346643
JP,2,jewelry,0.9762766818993396
JP,3,jewelry,0.9572356648494832
JP,4,jewelry,0.943823266150466
JP,5,jewelry,0.9374112134749528
JP,6,jewelry,0.938655287365333
JP,7,jewelry,0.9474282525208548
JP,8,jewelry,0.9628328705470142
JP,9,jewelry,0.9832936633129038
JP,10,jewelry,1.0067180419929558
JP,11,jewelry,1.0307103226255263
JP,12,jewelry,1.0528167401524349
JP,1,oth,1.0128601466271263
JP,2,oth,0.9755720110481638
JP,3,oth,0.9513338281424032
JP,4,oth,0.9430752948846406
JP,5,oth,0.9517946296642495
JP,6,oth,0.976437916483262
JP,7,oth,1.014026492992136
JP,8,oth,1.06001698482518
JP,9,oth,1.1088504684724712
JP,10,oth,1.1546243847419069
JP,11,oth,1.1918059878604002
JP,12,oth,1.2159010948028517
JP,1,rtw,0.8932614175940866
JP,2,rtw,0.8383604542187898
JP,3,rtw,0.8096627027922085
JP,4,rtw,0.8100144759091579
JP,5,rtw,0.8393808838586495
JP,6,rtw,0.8948492950700547
JP,7,rtw,0.9709182179770632
JP,8,rtw,1.0600429522954684
JP,9,rtw,1.1533838906975316
JP,10,rtw,1.2416832525078112
JP,11,rtw,1.3161832928459207
JP,12,rtw,1.3694949170062674
JP,1,shoes,0.957504197349722
JP,2,shoes,0.898011302939061
JP,3,shoes,0.8595899051901272
JP,4,shoes,0.8463501838750312
JP,5,shoes,0.8597084756688019
JP,6,shoes,0.8982357596628433
JP,7,shoes,0.9578105287641172
JP,8,shoes,1.0320596933602086
JP,9,shoes,1.1130403509983444
JP,10,shoes,1.1920894888664593
JP,11,shoes,1.2607507210310844
JP,12,shoes,1.3116789214919802
JP,1,slg,0.9468788779914464
JP,2,slg,0.9307688070257676
JP,3,slg,0.9234109914203305
JP,4,slg,0.9254938436670362
JP,5,slg,0.9368224877887871
JP,6,slg,0.956336992340174
JP,7,slg,0.9822115397923484
JP,8,slg,1.0120252538086958
JP,9,slg,1.0429887014570194
JP,10,slg,1.0722048783413818
JP,11,slg,1.0969402583432522
JP,12,slg,1.1148805479958783
JP,1,sport,0.6992615512576248
JP,2,sport,0.6952836446081536
JP,3,sport,0.7302403361367649
JP,4,sport,0.8009465924761546
JP,5,sport,0.9009601052764672
JP,6,sport,1.0211682737302268
JP,7,sport,1.1506184873126557
JP,8,sport,1.2775160586081198
JP,9,sport,1.3902988808744792
JP,10,sport,1.4786908943226356
JP,11,sport,1.5346383759048818
JP,12,sport,1.5530437437888458
"""

# ======= REF TABLE NAMES =======
DIM_LOC_TBL = "tb_analytics.dy_gcs_dimlocationinfo"
DIM_EVENT_TBL = "tb_analytics.dy_fpa_dimeventcalendar"
DIM_HOLIDAY_TBL = "tb_analytics.dy_fpa_dimholiday"

# ======= CREATED TABLE NAMES =======
FEAT_ALL_TRANS_TBL = "tb_analytics.dy_fpa_feat_alltrans"
FEAT_ALL_ITEMS_TBL = "tb_analytics.dy_fpa_feat_dimitem"
FEAT_ALL_PRODCATAUR_TBL = "tb_analytics.dy_fpa_feat_prodcataur"
FEAT_ALL_PRODCATAUR_WEIGHTS_TBL = "tb_analytics.dy_fpa_feat_prodcataur_weights"
FEAT_REGION_GCID_TBL = "tb_analytics.dy_fpa_feat_gcidtrans_{region}"
FEAT_REGION_MERGED = "tb_analytics.dy_fpa_feat_merged_{region}"

DIM_EMAIL_CAMPAIGN_TBL = "tb_analytics.dy_fpa_dimstcampaign"
DIM_EMAIL_RECIPIENT_TBL = "tb_analytics.dy_fpa_dimstrecipient"
DIM_EMAIL_RECIPIENT_TMP_TBL = DIM_EMAIL_RECIPIENT_TBL + "_tmp"
DIM_EMAIL_RECIPIENT_MATCHED_TBL = DIM_EMAIL_RECIPIENT_TBL + "_matched"
FACT_EMAIL_ACTIVITY_MATCHED_TBL = "tb_analytics.dy_fpa_factstactivity_matched"
FEAT_EMAIL_ACTIVITY_MATCHED_CAMLEVEL_TBL = "tb_analytics.dy_fpa_factstactivity_camlevel"
FEAT_EMAIL_CLICKURLTYPE_TBL = "tb_analytics.dy_fpa_dimstclickurltype"
FEAT_EMAIL_REGION_GCID_TBL = "tb_analytics.dy_fpa_feat_emailactivity_{region}"

# ======= FINAL TABLE NAMES AND SCHEMA =======
FINAL_FPA_SCORE_RAW = "tb_analytics.dy_fpa_score_final_raw"
FINAL_FPA_SCORE_RAW_SCHEMA = StructType([
    StructField("sk_goldencustomerid", IntegerType(), False),
    StructField("region", StringType(), False),
    StructField("handbags_high", DoubleType(), True),
    StructField("handbags_low", DoubleType(), True),
    StructField("handbags_mid", DoubleType(), True),
    StructField("jewelry_high", DoubleType(), True),
    StructField("jewelry_low", DoubleType(), True),
    StructField("jewelry_mid", DoubleType(), True),
    StructField("oth_high", DoubleType(), True),
    StructField("oth_low", DoubleType(), True),
    StructField("oth_mid", DoubleType(), True),
    StructField("rtw_high", DoubleType(), True),
    StructField("rtw_low", DoubleType(), True),
    StructField("rtw_mid", DoubleType(), True),
    StructField("shoes_high", DoubleType(), True),
    StructField("shoes_low", DoubleType(), True),
    StructField("shoes_mid", DoubleType(), True),
    StructField("slg_high", DoubleType(), True),
    StructField("slg_low", DoubleType(), True),
    StructField("slg_mid", DoubleType(), True),
    StructField("sport_high", DoubleType(), True),
    StructField("sport_low", DoubleType(), True),
    StructField("sport_mid", DoubleType(), True),
    StructField("refresh_dt", StringType(), True)
])

FINAL_FPA_SCORE_RANKED = "tb_analytics.dy_fpa_score_final_ranked"
FINAL_FPA_SCORE_RANKED_SCHEMA = StructType([
    StructField("sk_goldencustomerid", IntegerType(), False),
    StructField("region", StringType(), False),
    StructField("handbags_high", IntegerType(), True),
    StructField("handbags_low", IntegerType(), True),
    StructField("handbags_mid", IntegerType(), True),
    StructField("jewelry_high", IntegerType(), True),
    StructField("jewelry_low", IntegerType(), True),
    StructField("jewelry_mid", IntegerType(), True),
    StructField("oth_high", IntegerType(), True),
    StructField("oth_low", IntegerType(), True),
    StructField("oth_mid", IntegerType(), True),
    StructField("rtw_high", IntegerType(), True),
    StructField("rtw_low", IntegerType(), True),
    StructField("rtw_mid", IntegerType(), True),
    StructField("shoes_high", IntegerType(), True),
    StructField("shoes_low", IntegerType(), True),
    StructField("shoes_mid", IntegerType(), True),
    StructField("slg_high", IntegerType(), True),
    StructField("slg_low", IntegerType(), True),
    StructField("slg_mid", IntegerType(), True),
    StructField("sport_high", IntegerType(), True),
    StructField("sport_low", IntegerType(), True),
    StructField("sport_mid", IntegerType(), True),
    StructField("fpa_score", DoubleType(), True),
    StructField("refresh_dt", StringType(), True)
])
