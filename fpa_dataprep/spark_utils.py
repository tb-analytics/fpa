import pyspark.sql.functions as f
from pyspark.sql import DataFrame
from functools import reduce
import re
import os
from shutil import make_archive
import numpy as np
from databricks import koalas as ks
from typing import Tuple
import logging

logger = logging.getLogger(__name__)


def save_table(df, table_name, table_format="hive", mode="overwrite"):
    df.write \
        .format(table_format) \
        .mode(mode) \
        .saveAsTable(table_name)
    logger.info(f"[func save_table] {table_name} being saved.")
    return


def refresh_table(spark, table_name):
    spark.sql(f"refresh table {table_name}")


def unionAll(*dfs):
    return reduce(DataFrame.unionAll, dfs)


def exploding_map_to_columns(df, keys, map_col_name):
    key_cols = list(map(lambda col_name: f.col(map_col_name).getItem(col_name).alias(str(col_name)), keys))
    final_cols = [f.col('*')] + key_cols
    return df.select(final_cols)


def add_user_module(sc):
    root_dir = os.path.expanduser(os.path.join("~", "source"))
    base_dir = os.path.expanduser(os.path.join("fpa_dataprep"))
    zip_location = os.path.expanduser(os.path.join("~", "source", "src"))
    zip_loc = make_archive(zip_location, 'zip', root_dir, base_dir)

    sc.addPyFile(zip_loc)
    logger.info(f"[func add_user_module] {zip_loc} being added to sparkContext.")
    return


def make_dir_if_not_exist(pathdir):
    if not os.path.exists(pathdir):
        os.makedirs(pathdir)
    return pathdir


def ks_dataframe_chunks(kdf: ks.DataFrame, chunks=10):
    from math import ceil
    chunksize = ceil(len(kdf) / chunks)
    for i in range(chunks):
        sample = kdf[i*chunksize: (i+1)*chunksize]
        logger.info(f"[func ks_dataframe_chunks] returning chunk {i+1}/{chunks} of {len(sample)} samples ...")
        yield sample


def ks_dataframe_chunks_on(kdf: ks.DataFrame, split_on: str, chunks=10):
    from math import ceil
    logger.info(f"[ks_dataframe_chunks_on] splitting {chunks} chunks...")
    split_series = kdf[split_on].unique()
    chunksize = ceil(len(split_series) / chunks)
    for i in range(chunks):
        split_series_chunk = split_series[i*chunksize: (i+1)*chunksize]
        sample = ks.merge(kdf, split_series_chunk, on=split_on)
        yield sample


def ks_dataframe_split(kdf: ks.DataFrame, frac: float = .7) -> Tuple[ks.DataFrame, ks.DataFrame]:
    sdf = kdf.to_spark()
    s1, s2 = sdf.randomSplit([frac, 1-frac])
    k1 = s1.to_koalas()
    k2 = s2.to_koalas()
    return k1, k2


def ks_dataframe_split_on(kdf: ks.DataFrame, split_on: str, frac: float = .7) -> Tuple[ks.DataFrame, ks.DataFrame]:
    logger.info(f"[ks_dataframe_split_on] Splitting dataframe on {split_on}, frac={frac}")
    split_series = ks.DataFrame(kdf[split_on].unique()).to_spark()
    p1, p2 = split_series.randomSplit([frac, 1-frac])
    k1 = ks.merge(kdf, p1.to_koalas(), on=split_on)
    k2 = ks.merge(kdf, p2.to_koalas(), on=split_on)
    logger.info(f"[ks_dataframe_split_on] part1 size: {k1.shape[0]}, part2 size:{k2.shape[0]}")
    return k1, k2
