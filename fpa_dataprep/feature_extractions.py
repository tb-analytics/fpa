import databricks.koalas as ks
import pandas as pd
import numpy as np
import pyspark
from functools import reduce
from collections import OrderedDict
from typing import Dict, Any, Optional, List, Tuple, Union
from io import StringIO
import random
import itertools
import logging
from fpa_dataprep.spark_utils import ks_dataframe_chunks_on, ks_dataframe_split_on, save_table
from fpa_dataprep.data_transform_utils import transform_to_matrix
import fpa_dataprep.constant as C

logger = logging.getLogger(__name__)


"""
====== NOTES ======
Main transaction data is pivoted into main feature sets:
{
    "df": ks.DataFrame, the feature dataframe
    "cols": List[str], column names of desired features, note that indexing
                       columns should be excluded
    "idx": List[str], column names to perform join to the main feature sets / index of the feature
}

Side feature sets are following below format
{
    "df": ks.DataFrame, the feature dataframe
    "cols": List[str], column names of desired features, note that indexing
                       columns should be excluded
    "join_on": List[str], column names to perform join to the main feature sets
}
"""


def pivot_categorical_variable(df, cat_col: str, 
                               guarantee_columns: Optional[List[str]] = None) -> Dict[str, Dict[str, Any]]:
    _df = df.groupby(["sk_goldencustomerid", "prodcatbucket", "dayid"]).first().reset_index()  # for nunique
    final = _df.pivot_table(columns=cat_col, index=["sk_goldencustomerid", "prodcatbucket"], aggfunc='count', values="dayid")
    final.columns = [f"{cat_col}_{col}" for col in final.columns]
    
    if guarantee_columns:
        for col in guarantee_columns:
            if col not in final.columns:
                final[col] = 0
        final = final[guarantee_columns]
    
    logger.info(f"[func pivot_categorical_variable] Categorical feature {cat_col} preperation done, with {final.shape[0]} records.")
    return {cat_col: {"df": final.cache(), "cols": final.columns.to_list(), "idx": final.index.names}}


def stats_numeric_variable(df, aggfuncdict: Dict[str, List[str]], 
                           dataset_label: str = "numeric",
                           new_column_names: Optional[List[str]] = None) -> Dict[str, Dict[str, Any]]:
    _df = df.groupby(["sk_goldencustomerid", "prodcatbucket"]).agg(aggfuncdict)
    computed_vals = ["_".join(items) for items in _df.columns]
    if new_column_names:
        assert len(new_column_names) == len(_df.columns), "Column names length mismatch."
        _df.columns = new_column_names
    else:
        _df.columns = computed_vals
    
    logger.info(f"[func stats_numeric_variable] Numeric feature preperation done, with {_df.shape[0]} records.")
    return {dataset_label: {"df": _df.cache(), "cols": _df.columns.to_list(), "idx": _df.index.names}}


def prep_date_features(dt: pd._libs.tslibs.timestamps.Timestamp, region: str) -> Dict[str, Any]:
    # add region specific seasonality adjustments
    # load seasonality file
    from fpa_dataprep.constant import SEASONALITY_ADJ_CSV_STRING

    seasonality = pd.read_csv(StringIO(SEASONALITY_ADJ_CSV_STRING), keep_default_na=False)
    
    basedf = seasonality.loc[
        (seasonality["month"] == dt.month) & (seasonality["region"] == region), 
        ["month", "prodcat", "adjusted_param"]]
    
    # create prodcatbucket
    m = pd.DataFrame({
        "month": [dt.month] * 3,
        "tier": ["low", "mid", "high"]
    })
    basedf = pd.merge(basedf, m, on="month")
    basedf["prodcatbucket"] = basedf["prodcat"] + "_" + basedf["tier"]
    
    # append fp adjusted param
    # basedf["fp_adjusted"] = fp_offset(dt)
    
    # add one hot encoding month
    for mth in np.arange(12):
        basedf[f"{mth+1}"] = int(mth == dt.month - 1)
    
    # drop not useful columns
    basedf = basedf.drop(["month", "prodcat", "tier"], axis=1)
    
    return {"df": ks.from_pandas(basedf).cache(),
            "cols": basedf.columns.drop(["prodcatbucket"]).to_list(),   # only column features are saved in the dict
            "join_on": ["prodcatbucket"]}


def prep_email_features(spark, region, read_cache=False):
    from fpa_dataprep.get_raw_data import fetch_email_gcid_level_data
    if read_cache:
        logger.info("[func prep_email_features] Reading cache from "+C.FEAT_EMAIL_REGION_GCID_TBL.format(region=region))
        kdf = ks.read_table(C.FEAT_EMAIL_REGION_GCID_TBL.format(region=region))
    else:
        kdf = fetch_email_gcid_level_data(spark, region)
    if kdf is not None:
        return {
            "df": kdf.cache(),
            "cols": kdf.columns.drop(["sk_goldencustomerid"]).to_list(),
            "join_on": ["sk_goldencustomerid"]
        }
    else:
        logger.warning(f"[func prep_email_features] {region} GCID level email behavior data is empty, return None.")
        return


def prep_main_trans_features(spark, region, mode, target_name):
    from fpa_dataprep.get_raw_data import fetch_region_gcid_trans
    # set up transaction days

    kdf_gcid_trans = fetch_region_gcid_trans(spark, region, mode)
    
    train_raw = kdf_gcid_trans.loc[kdf_gcid_trans["timeframe"] == "TRAIN"]
    test_raw = kdf_gcid_trans.loc[kdf_gcid_trans["timeframe"] == "TEST"]

    features_holder = OrderedDict()

    # process numeric features
    _df = stats_numeric_variable(train_raw, {
        "sk_itemid": ["count"],
        "ordernumber": ["nunique"],
        "fp_pct": ["min", "max", "mean", "stddev"],
        "net_spend": ["min", "max", "mean", "stddev"],
        "msrp": ["min", "max", "mean", "stddev"],
        "mkdntaken": ["sum"],
        "dscttaken": ["sum"],
        "item_avg_fp_pct": ["min", "max", "mean", "stddev"],
        "item_popularity": ["min", "max", "mean", "stddev"]
    })
    features_holder.update(_df)

    # process categorical features
    guarantee_cat_values = OrderedDict()
    guarantee_cat_values["channel"] = ["EC", "RF"]
    guarantee_cat_values["event"] = ["SAS SUMMER", "SAS WINTER", "PRIVATE SALE - MAR", "PRIVATE SALE - WINTER",
                                     "FALL EVENT", "SPRING EVENT", "HOLIDAY EVENT", "FULL PRICE"]
    guarantee_cat_values["holiday"] = ["cyber_monday", "thanksgiving", "new_years", "july_4th", "christmas", "memorial_day",
                                       "mothers_day", "valentines_day", "labor_day", "presidents_day"]
    guarantee_cat_values["calendarmonthnum"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    guarantee_cat_values["covid_first6mo"] = [0, 1]
    
    for cat_variable, cols in guarantee_cat_values.items():
        features_holder.update(pivot_categorical_variable(train_raw, cat_variable,
                                                          guarantee_columns=[f"{cat_variable}_{col}" for col in cols]))
    
    # process target score
    logger.info("[func prep_main_trans_features] processing target (if mode is scored, it has no usage/meaning).")
    features_holder.update(stats_numeric_variable(test_raw, {"fp_pct": ["mean"]}, dataset_label="target",
                                                  new_column_names=[target_name]))
    return features_holder


def prep_prodcataur_features(region):
    from fpa_dataprep.get_raw_data import get_prodcataur_info
    prodcataur_info = get_prodcataur_info(region)
    res = {
        "df": prodcataur_info.cache(),
        "cols": prodcataur_info.columns.drop(["prodcatbucket", "prodcatbucket_idx"]).to_list(),
        "join_on": ["prodcatbucket"]
    }
    n_cat = prodcataur_info["prodcatbucket_idx"].max()
    return res, n_cat


def get_all_features(spark: pyspark.sql.SparkSession, dt: pd._libs.tslibs.timestamps.Timestamp, target_name: str,
                     region: str, mode: str):
    # ================== PREP MAIN TRANSACTIONAL FEATURES ================== 
    features_holder = prep_main_trans_features(spark, region, mode, target_name)  # contains y_train
    
    # merge everything together
    main_ft_dfs = [features_holder[grp]["df"] for grp in list(features_holder.keys()) if grp != 'target']
    train = ks.concat(main_ft_dfs, axis=1, join="inner")
    
    if mode == "score":
        join_method = "left"
    elif mode == "train":
        join_method = "outer"
    else:
        raise ValueError("[func get_all_features] mode can only be either `train` or `score`.")
    train = ks.merge(train, features_holder["target"]["df"], how=join_method, left_index=True, right_index=True)
    train[target_name+"_new"] = (~(train[target_name] > 0)).astype(int) \
                                * train["fp_pct_mean"] * random.uniform(.8, 1.2) \
                                + train[target_name]  # use LY's 80% ~ 120% to fill in the non-purchases.

    logger.info(f"[func get_all_features] Main trans features merged, with {train.shape[0]} records.")
    
    # ================== PREP SIDE FEATURES ================== 
    sp_features_holder = OrderedDict()
    logger.info(f"[func get_all_features] Adding prodcataur_features ...")
    sp_features_holder["prodcataur_info"], n_cat = prep_prodcataur_features(region)
    logger.info(f"[func get_all_features] Adding seasonality_features ...")
    sp_features_holder["date_features"] = prep_date_features(dt, region)
    # Note: JP email not available, return None
    logger.info(f"[func get_all_features] Adding email_features ...")
    email_features = prep_email_features(spark, region, read_cache=False)
    if email_features is not None:
        sp_features_holder["email_features"] = email_features

    # create gcid_X_prodcataur table, left merge all train/side features
    train = train.reset_index()  # move gcid/prodcataur into feature columns
    _allcust = train.groupby("sk_goldencustomerid", as_index=False).agg("first").loc[:, ["sk_goldencustomerid"]].to_spark()
    _allprodcataur = sp_features_holder["prodcataur_info"]["df"].loc[:, ["prodcatbucket"]].to_spark()
    _merged_idx = _allcust.crossJoin(_allprodcataur).to_koalas()
    train = ks.merge(_merged_idx, train, on=_merged_idx.columns.to_list(), how="left")

    # merge side features
    _train = reduce(lambda x, y: {"df": ks.merge(x["df"], y["df"], on=y["join_on"], how="left")}, 
                    sp_features_holder.values(), {"df": train})["df"]
    
    # logging
    n_cust = _train["sk_goldencustomerid"].nunique()
    logger.info(f"[func get_all_features] Side features merged, with {_train.shape[0]} records.")
    logger.info(f"[func get_all_features] Total customers after merging: {n_cust}.")

    save_table(_train.to_spark(), C.FEAT_REGION_MERGED.format(region=region))
    new_train = ks.read_table(C.FEAT_REGION_MERGED.format(region=region))

    _mainfeat = reduce(lambda a, b: a + b,
                       [features_holder[grp]["cols"] for grp in list(features_holder.keys()) if grp != 'target'])
    _sidefeat = reduce(lambda a, b: a + b, [i["cols"] for _, i in sp_features_holder.items()])
    feat_sets = _mainfeat + _sidefeat
    logger.info(f"func [get_all_features] features including {feat_sets} for {n_cat} buckets.")
    return new_train, feat_sets, n_cat


def _sample_without_replacement(df: pd.DataFrame, n_sample: int):
    logger.info(f"[func _sample_with_replacement] Sample {n_sample} GCID without replacement from each prodcatbucket, "
                 "prodcatbucket with less samples will be drawn as a whole ...")
    
    # per bucket generate a random row number for each gc
    mask = df.groupby("valid") \
        .transform(lambda x: np.random.choice(np.arange(len(x)), size=len(x), replace=False)) \
        ["sk_goldencustomerid"] <= n_sample
    sampled_cust = df[mask].groupby("sk_goldencustomerid", as_index=False).first().loc[:, ["sk_goldencustomerid"]]
    sampled = pd.merge(df, sampled_cust, on="sk_goldencustomerid", how="inner")
    
    return sampled


def _sample_with_replacement(df: pd.DataFrame, n_sample: int):
    logger.info(f"[func _sample_with_replacement] Sample {n_sample} GCID with replacement from each prodcatbucket ...")
    
    # relabel gcid to treat duplicate customers as a new one
    t = df.groupby("valid").apply(lambda x: x.sample(n=n_sample, replace=True)[["sk_goldencustomerid"]]).reset_index()
    t["rnk"] = t.groupby(["valid", "sk_goldencustomerid"]).transform(lambda x: np.arange(len(x)) + 1)
    t["new_gcid"] = (t["rnk"].astype(str) + t["sk_goldencustomerid"].astype(int).astype(str)).astype(int)

    sampled_cust = t.groupby("new_gcid", as_index=False).first().loc[:, ["new_gcid", "sk_goldencustomerid"]]
    sampled = pd.merge(df, sampled_cust, on="sk_goldencustomerid", how="inner") \
              .rename(columns={"sk_goldencustomerid": "orig_sk_goldencustomerid", "new_gcid": "sk_goldencustomerid"})
    
    return sampled


def sample_each_prodcatbucket(feat_df: pd.DataFrame, sample_qtle=.5, with_replacement=True):
    """
    sample_qtle: n_sample will be determined by the quantile value of available prodcatbucket gcids for all prodcatbuckets
    """
    # stratified sampling of transactions, 2+ prodcatbucket customers, valid column excludes non purchased pcbk
    feat_df['valid'] = feat_df.loc[feat_df["sk_itemid_count"] > 0, "prodcatbucket"]
    gc = feat_df.groupby("sk_goldencustomerid", as_index=False)[["valid"]].count()
    usefulgc = gc[gc["valid"] >= 2][["sk_goldencustomerid"]]
    df = pd.merge(feat_df, usefulgc, on="sk_goldencustomerid", how="inner")
    
    n_sample = int(df.groupby("valid").count()["sk_goldencustomerid"].quantile(sample_qtle))

    if with_replacement:
        sampled = _sample_with_replacement(feat_df, n_sample)
    else:
        sampled = _sample_without_replacement(feat_df, n_sample)
    
    return sampled


def datasets_for_training(train: ks.DataFrame, feat_sets: List[str], n_cat: int,
                          target_name: str, sample_qtle: float = .5, 
                          with_replacement: bool = True, train_size: float = .7):
    """
    :param train:
    :param feat_sets:
    :param n_cat:
    :param target_name:
    :param n_samples:
    :param train_size:
    :return:
    """
    from fpa_dataprep.data_transform_utils import yTransformer, dense_to_coo

    # stratified sampling of transactions, 2+ prodcatbucket customers
    sampled = sample_each_prodcatbucket(train.to_pandas(), sample_qtle, with_replacement)
    (train_X, train_y, test_X, test_y), idx = transform_to_matrix(sampled, n_cat, feat_sets,
                                                                  target_name+"_new", train_size)

    y_transformer = yTransformer()
    y_transformer.fit(train_y, 0.2, 1)
    train_y_trans = y_transformer.transform(train_y)
    test_y_trans = y_transformer.transform(test_y)

    datasets = {
        "idx": idx,
        "train_X": dense_to_coo(train_X),
        "train_y": train_y_trans,
        "train_y_raw": train_y,
        "test_X": dense_to_coo(test_X),
        "test_y": test_y_trans,
        "test_y_raw": test_y,
    }
    logger.info(f"[func datasets_for_training] Train: total {len(train_y)} samples.")
    logger.info(f"[func datasets_for_training] Test: total {len(test_y)} samples.")
    yield datasets


def datasets_for_scoring(train: ks.DataFrame, feat_sets: List[str], n_cat: int,
                         target_name: str, chunks: int = 1):
    from fpa_dataprep.data_transform_utils import dense_to_coo

    chunks = int(chunks)
    assert chunks >= 1, "[datasets_for_scoring] chunks must be a positive integer."
    
    for sampled in ks_dataframe_chunks_on(train, "sk_goldencustomerid", chunks):
        (train_X, train_y), idx = transform_to_matrix(sampled.to_pandas(), n_cat, feat_sets, target_name, None)
        datasets = {
            "train_X": dense_to_coo(train_X),
            "train_y": train_y,
            "idx": idx
        }
        logger.info(f"[func datasets_for_scoring] Total {len(train_y)} customers.")
        yield datasets
