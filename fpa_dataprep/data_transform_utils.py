import pyspark.sql.functions as f
from pyspark.sql.types import FloatType
import pandas as pd
from databricks import koalas as ks
import numpy as np
from typing import Optional, List, Tuple, Union, Dict
from scipy import stats
import math
import re
import sparse
import logging

logger = logging.getLogger(__name__)


class yTransformer:
    def __init__(self, func=stats.norm):
        self.func = func
        self.args = None
    
    def fit(self, y, lower_limit: Optional[float] = None, upper_limit: Optional[float] = None):
        if lower_limit:
            y = y[y > lower_limit]
        elif upper_limit:
            y = y[y < upper_limit]
        else:
            pass
        
        self.args = self.func.fit(y)
        return
    
    def transform(self, array: np.ndarray):
        if self.args:
            transformed_array = self.func(*self.args).cdf(array)
            return transformed_array
        else:
            raise ValueError("args haven't been fitted.")
    
    def reverse_transform(self, array: np.ndarray):
        if self.args:
            transformed_array = self.func(*self.args).ppf(array)
            return transformed_array
        else:
            raise ValueError("args haven't been fitted.")
    
    def save(self, fileloc: str):
        import pickle
        with open(fileloc, 'wb+') as f:
            pickle.dump(self, f)


def _result_cleanup(kdf: ks.DataFrame, start_idx: int) -> ks.DataFrame:
    kdf = kdf.loc[ks.notnull(kdf["val"]) & (kdf["val"].abs() > 0), :]
    kdf["col_idx"] = kdf["col_idx"] - kdf["col_idx"].min() + start_idx
    return kdf


def transform_to_rcv_plain(df: ks.DataFrame, row_idx_col: List[str], 
                           feature_columns: List[str], start_idx: int, verbose: bool = True) -> Tuple[ks.DataFrame, int]:
    n_features = len(feature_columns)

    # unpivot values to stacked values
    kdf = ks.melt(df, id_vars=row_idx_col, value_vars=feature_columns, var_name="col_str", value_name="val")
    idx = ks.DataFrame({
        "col_str": feature_columns,
        "col_idx": np.arange(n_features) + 1
    })
    final = ks.merge(kdf, idx, on="col_str").loc[:, row_idx_col+["col_idx", "val"]]
    final = _result_cleanup(final, start_idx)
    start_idx += n_features
    if verbose:
        logger.info(f"[func transform_to_rcv_plain] {feature_columns} being processed, pointer moving to {start_idx}.")
        logger.debug("[func transform_to_rcv_plain] min col_idx: " + str(final["col_idx"].min()) + ", max col_idx: " + str(final["col_idx"].max()))
    return final, start_idx


def transform_to_rcv_categorical(df: ks.DataFrame, row_idx_col: List[str], 
                                 feature_columns: List[str], start_idx: int) -> Tuple[ks.DataFrame, int]:
    df["val"] = 1
    for fc in feature_columns:
        
        final = df.loc[:, row_idx_col+[fc, "val"]].rename(columns={fc: "col_idx"})
        final = _result_cleanup(final, start_idx)
        # return pointer
        n_cat = df[fc].max() - df[fc].min() + 1
        start_idx += n_cat
    logger.info(f"[func transform_to_rcv_categorical] {feature_columns} being processed, pointer moving to {start_idx}.")
    logger.debug("[func transform_to_rcv_categorical] min col_idx: " + str(final["col_idx"].min()) + ", max col_idx: " + str(final["col_idx"].max()))
    return final, start_idx


def transform_to_rcv_multiplier(df: ks.DataFrame, row_idx_col: List[str], 
                                feature_columns: List[str], start_idx: int, multiplier_col: str) -> Tuple[ks.DataFrame, int]:
    df_tr, pi = transform_to_rcv_plain(df, row_idx_col, feature_columns, 1, verbose=False)
    df_m = ks.merge(df_tr, df.loc[:, row_idx_col+[multiplier_col]], on=row_idx_col)
    df_m["new_col_idx"] = df_m["col_idx"] + (df_m[multiplier_col] - 1) * len(feature_columns)
    final = df_m.loc[:, row_idx_col+["new_col_idx", "val"]].rename(columns={"new_col_idx": "col_idx"})
    final = _result_cleanup(final, start_idx)
    start_idx += (pi - 1) * df_m[multiplier_col].max()
    logger.info(f"[func transform_to_rcv_multiplier] {feature_columns} being processed, pointer moving to {start_idx}.")
    logger.debug("[func transform_to_rcv_multiplier] min col_idx: " + str(final["col_idx"].min()) + ", max col_idx: " + str(final["col_idx"].max()))
    return final, start_idx


def parsing_email_campaign(nomen, campaign_name):
    res = dict()
    campaign_name = "_{0}_".format(campaign_name.replace(' ', '_'))
    for area in nomen:
        matching_str = "_({0})_".format("|".join(nomen[area]))
        matched_text = re.findall(matching_str, campaign_name, flags=re.I)
        if matched_text:
            res[area] = matched_text[0]
            if ("type" in res) and (res['type'].lower() != "trigger"):  # if triggered email, then don't match in order
                campaign_name = re.split(res[area], campaign_name)[-1]
        else:
            res[area] = "None"
    return res


def parsing_url_type(word_matching, url):
    if not url:
        return "None"
    
    for urltype in word_matching:
        matching_str = "({0})".format("|".join(word_matching[urltype]))
        matched_text = re.findall(matching_str, url, flags=re.I)
        if matched_text:
            return urltype
    
    return "others"


def erf(x):
    # save the sign of x
    sign = 1 if x >= 0 else -1

    x = abs(x)

    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p * x)
    y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * math.exp(-1 * x ** 2)
    
    return sign * y  # erf(-x) = -erf(x)


def norm_cdf(x, mu=0, std=1):
    val = 0.5 * (1 + erf((x - mu) / (std * 2 ** 0.5)))
    return val


def survival_function(x):
    return 1 - norm_cdf(x)


@f.udf(FloatType())
def two_sample_proportion_ztest(x1, n1, x2, n2):
    try:
        p1 = x1 / n1
        p2 = x2 / n2
        p_pool = (x1 + x2) / (n1 + n2)

        z = (p1 - p2) / ((p_pool * (1 - p_pool)) * (1 / n1 + 1 / n2)) ** .5
        p = survival_function(abs(z))
        return p
    except Exception:
        return


def _get_prodcat_headers(region):
    from fpa_dataprep.feature_extractions import prep_prodcataur_features
    _res = prep_prodcataur_features(region)
    _allprodcataur = _res["df"].to_pandas()
    _feat_cols = _res["cols"]
    n_cat = _allprodcataur["prodcatbucket_idx"].max()
    
    p = _allprodcataur.sort_values(by="prodcatbucket_idx").loc[:, ["prodcatbucket_idx"]+_feat_cols].reset_index(drop=True)
    col_orders = ["prodcatbucket_idx_{0:d}".format(i+1) for i in range(n_cat)] + _feat_cols
    data_headers = pd.get_dummies(p, columns=["prodcatbucket_idx"]).T.loc[col_orders, :]
    data_headers["col_idx"] = np.arange(data_headers.shape[0])

    return data_headers, len(_feat_cols)


# below functions are for scoring expansion
def expanding_for_each_cat(train_X, train_y, train_idx, region, sample_rate=None):
    from scipy.stats import rankdata
    # loading into pandas dataframe
    train_X = pd.DataFrame(train_X, columns=["row_idx", "col_idx", "val"]).astype({"row_idx": int, "col_idx":int})
    train_idx = pd.DataFrame(np.hstack([train_idx, train_y.reshape(-1, 1)]), columns=["gcid", "prodcatid", "row_idx", "target"]) \
        .astype({"gcid": int, "prodcatid":int, "row_idx":int})
    n_cat = train_idx["prodcatid"].max()
    data_headers, n_extra_feat = _get_prodcat_headers(region)

    
    if sample_rate:
        logger.info(f"[func expanding_for_each_cat] sample_rate is {sample_rate}, orig. samples: {train_idx.shape[0]}")
        sampled_gc = train_idx.groupby("prodcatid", as_index=False).sample(frac=0.2)
        sampled_gc = sampled_gc.groupby("gcid", as_index=False).first().loc[:, ["gcid"]]
        train_idx = pd.merge(train_idx, sampled_gc, on="gcid", how="inner")
        logger.info(f"[func expanding_for_each_cat] new samples: {sampled_gc.shape[0]} customers, {train_idx.shape[0]} rows")
    
    # adding new row and dedup
    train_idx["new_row_idx"] = rankdata(train_idx["gcid"], method="dense") - 1
    dedup_rows = train_idx.groupby("gcid", as_index=False).first()
    
    # getting unchanged part for each gc
    unchanged_part_df = train_X.loc[train_X["col_idx"] >= n_cat + n_extra_feat].merge(dedup_rows, how="inner", on="row_idx")
    unchanged_part = unchanged_part_df.loc[:, ["new_row_idx", "col_idx", "val"]].to_numpy()
    
    # for each new row number, adding prodcat id
    dedup_rows = dedup_rows.loc[:, ["gcid", "prodcatid", "new_row_idx"]]
    row_idx_arr = dedup_rows["new_row_idx"].to_numpy().reshape(-1, 1)
    n_rows = len(row_idx_arr)
    
    
    for _c in range(n_cat):
        ele = data_headers.loc[data_headers[_c] != 0, ["col_idx", _c]].to_numpy()
        new_arr = np.hstack([np.repeat(row_idx_arr, ele.shape[0]).reshape(-1, 1), np.tile(ele, (n_rows, 1))])
        _train_X_np = np.vstack([new_arr, unchanged_part])

        # updating train_idx
        dedup_rows["prodcatid"] = _c + 1  # row coo matrix starts at 0, for prodcatid starts at 1

        # getting train_y
        new_train_idx_df = pd.merge(dedup_rows, train_idx.loc[:, ["gcid", "prodcatid", "target"]], how="left", on=["gcid", "prodcatid"]).fillna({"target": .0})
        _train_idx_np = new_train_idx_df.loc[:, ["gcid", "prodcatid", "new_row_idx"]].to_numpy()
        _train_y_np = new_train_idx_df.loc[:, ["target"]].to_numpy()
        
        assert np.unique(_train_idx_np[:, 0]).shape[0] == _train_y_np.shape[0] == _train_idx_np.shape[0]
        
        yield _train_X_np, _train_y_np, _train_idx_np


def train_test_split(X, y, sample_rate=None):
    import random
    if sample_rate is not None:
        population = y.shape[0]

        idx = list(range(population))
        random.shuffle(idx)
        cut = math.ceil(population * sample_rate)

        train_idx = idx[:cut]
        test_idx = idx[cut:]

        train_X = X[train_idx]
        train_y = y[train_idx]
        test_X = X[test_idx]
        test_y = y[test_idx]

        logger.info(train_X.shape)
        logger.info(test_X.shape)

        return train_X, train_y, test_X, test_y
    else:
        logger.info(X.shape)
        return X, y


def reshape_to_matrix(multi_df: pd.DataFrame) -> np.ndarray:
    """
    Needs a dataframe with two levels of index (gcid/prodcataur)
    Will reshape the dataframe to a matrix of (N_samples[gcid], M_categories[prodcataur], n_features[feat_sets])
    :param multi_df: pandas dataframe
    :return: numpy array
    """
    m, n = len(multi_df.index.levels[0]), len(multi_df.index.levels[1])
    arr = multi_df.to_numpy().reshape(m, n, -1)
    logger.info(f"[func reshape_to_matrix] Reshape dataframe to {arr.shape}")
    return arr


def preprocessing(df: pd.DataFrame, n_cat: int, feat_cols: List[str], target_col: str):
    """
    Expanding the dataframe to a cross product of gcid X prodcataur
    """
    from scipy.stats import rankdata
    df["gcid"] = df["gcid"] = rankdata(df["sk_goldencustomerid"], "dense") - 1
    idx = df.groupby("gcid").first().loc[:, ["sk_goldencustomerid"]].reset_index().to_numpy()

    _g = df["gcid"].unique()
    _p = np.arange(n_cat) + 1
    index = pd.MultiIndex.from_product([_g, _p], names=["gcid", "prodcatbucket_idx"])

    tr_df = pd.merge(pd.DataFrame(index=index),
                     df.set_index(["gcid", "prodcatbucket_idx"]),
                     how="left", left_index=True, right_index=True).fillna(0.)

    feat = tr_df.loc[:, feat_cols]
    target = tr_df.loc[:, [target_col]]
    return feat, target, idx


def transform_to_matrix(df: pd.DataFrame, n_cat: int, feat_cols: List[str], target_col: str, train_test_split_rate=.75):
    feat, target, idx = preprocessing(df, n_cat, feat_cols, target_col)
    feat.sort_index(inplace=True)
    target.sort_index(inplace=True)

    X = reshape_to_matrix(feat)
    y = reshape_to_matrix(target)

    return train_test_split(X, y, train_test_split_rate), idx


def dense_to_coo(mat: np.ndarray) -> Dict[str, np.ndarray]:
    mat_s = sparse.COO(mat)
    data_dict = {
        "data": mat_s.data,
        "shape": mat_s.shape,
        "fill_value": mat_s.fill_value,
        "coords": mat_s.coords
    }
    
    return data_dict


def coo_to_dense(data_dict: Dict[str, np.ndarray]) -> sparse.COO:
    coords = data_dict["coords"]
    data = data_dict["data"]
    shape = tuple(data_dict["shape"])
    fill_value = data_dict["fill_value"][()]
    
    dense_mat =  sparse.COO(
        coords=coords,
        data=data,
        shape=shape,
        sorted=True,
        has_duplicates=False,
        fill_value=fill_value,
    ).todense()

    return dense_mat
