
# Full-Price Affinity (FPA) Models



## 1. Background & Objectives
- Identify existing customers who are more likely to shop full price and higher AUR products
- Be leveraged as a third dimension of GCS in addition to retention and frequency
- Help acquisition of higher quality customers from look-alikes
- Be a tool to help target customers when we have specific product campaigns of different product categories and price points, e.g. Lee Radziwill Bag (best handbags) vs. Good Luck Trainer (better shoes)

## 2. Running Environments
- python 3.6, CML Runtime 2021.06 Standard version
- spark 2.4
- Runtime Image: docker.repository.cloudera.com/cdsw/ml-runtime-workbench-python3.6-standard:2021.06.1-b5

### 2.1 Required Packages
```txt
pandas==1.1.5
scikit-learn==0.24.2
scipy==1.5.4
numpy==1.19.5
matplotlib==3.3.4
torch==1.9.0
torchvision==0.10.0
pytorch-ignite==0.4.5
tensorboardX==2.4
tensorboard==2.5.0
sparse==0.12.0
pyyaml==5.4.1
koalas==1.8.1
```

### 2.2 Environment Variables
```
ST_API_KEY: ${Sailthru API Key}
ST_API_SECRET: ${Sailthru API Secret}
ARROW_PRE_0_15_IPC_FORMAT: 1
PYARROW_IGNORE_TIMEZONE: 1
```

### 2.3 Spark Configurations
```conf
# compute instances configs
spark.driver.memory=16g
spark.driver.memoryOverhead=2000
spark.executor.cores=2
spark.executor.memory=16g
spark.executor.memoryOverhead=3000
spark.executor.instances=10
spark.kubernetes.allocation.batch.size=5
spark.sql.autoBroadcastJoinThreshold=-1

# adls location configs
spark.yarn.access.hadoopFileSystems=abfs://data@clouderastorageaccountpd.dfs.core.windows.net

# writing table configs
spark.sql.legacy.allowCreatingManagedTableUsingNonemptyLocation=true
spark.sql.hive.caseSensitiveInferenceMode=NEVER_INFER

# other stuff
spark.sql.execution.arrow.enabled=true
spark.driver.maxResultSize=8g
```

## 3. Module Notes

### 3.1 `fpa_dataprep` Module
Spark module to prepare data needed for model training/scoring process. Needs spark 2.4 and koalas.
- `constant`: tables created/read, constant values needed (seasonality adjustments; email parsing text; URL parsing text) are saved here
- `get_raw_data`: mainly Spark SQLs to fetch raw data from `cip`, includes code using Sailthru API to fetch data.
- `feature_extractions`: core functions to create features
- `data_transform_utils`: helper functions to do feature engineering
- `spark_utils`: spark helper functions
- `email_subject.joblib`: pre-trained tf-idf model to classify subject lines

### 3.2 `ptframework` Module
PyTorch framework to build customized models, and leverage pytorch-ignite package to create model engine with built in evaluation process, which can be visualized in tensorboard. Runs locally with CPU multi-cores capability.
- `data_prep`: process *.npz files to prepare data loaders, has customized data loaders to enable multi-core processing and loading smaller chunks into memory at a time
- `model`: core module with customized `torch.nn` model classes, has customized loss functions
- `get_model`: helper wrappers to create an ignite engine with evaluators
- `evaluations`: helper evaluation functions
- `utils`: helper functions


## 4. Folders and Files
### 4.1 Folders
1. `models`: stores pytorch models in *.pt files. `models/production` folder contains production models for all regions.
2. `data`: training/scoring datasets in *.npz.
3. `scores`: output FPA scores in *.csv.
4. `runs`: tensorboard logging information.
5. `log`: logging location.
6. `metadata`: any graphs/files produced during training if saved.

### 4.2 Files

1.  `data_prep_main.py`: script to prepare data for training/scoring, starts a spark session, calls `fpa_dataprep` module. See helper docs for each argument.
2.  `train.py`: script to train models, calls `ptframework` module. See helper docs for each argument.
3.  `score.py`: script to train models, calls `ptframework` module. See helper docs for each argument.
4.  `fpa_model_setup.yaml`: production model configurations, tells `score.py` where to find models and corresponding parameters.
5.  `update_end_table.py`script to load FPA score files output from `score.py`, **end table names are set by `fpa_dataprep.constant` module**, starts a spark session, calls `fpa_dataprep` module. See helper docs for each argument.
6.  `job.py` and `refresh.sh`: job.py calls refresh.sh to run all the steps to complete a scoring cycle.
7.  `requirement.txt`: python packages needed to setup this project.
8.  `src.zip`: created during the each spark session that calls `fpa_dataprep` module, it's a zip file of all source code and will be used by each spark driver.

## 5. Using the Code
### 5.1 Scoring
- Manual: Open a workbench session, in the command prompt in iPython shell (where `$dt` is a date string like '2021-05-01'):

```python
import os
os.chdir("source")
!sh ./refresh.sh $dt
```

Or start a `Terminal Access` from a session or ssh-endpoint session:
```bash
cd source
sh ./refresh.sh $dt
```

- Automatically: `fpa_monthly` job has been setup in the CML `fpa` project. Runs on the 3th every month.
