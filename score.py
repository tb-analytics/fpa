import logging
import os
import glob
from datetime import date
from ptframework.data_prep import prep_features_from_file_cnn
from ptframework.utils import make_dir_if_not_exist, save_dataframe
import numpy as np
import pandas as pd
import argparse
import yaml
import importlib
import matplotlib.pyplot as plt


def reload_model(yaml_config_file, region):
    with open(yaml_config_file, "r") as f:
        modeldict_all = yaml.safe_load(f)
        modeldict = modeldict_all[region.upper()]

    _module = importlib.import_module(modeldict["model_module"])
    _cls = getattr(_module, modeldict["model_class"])
    model_dir = modeldict["model_dir"]
    modelparams = modeldict["modelparams"]
    
    logger.info(f"[func reload_model] Loading {model_dir} with parameters {modelparams}.")

    model = _cls.reload(model_dir, mode="score", **modelparams)
    return model, modelparams


if __name__ == '__main__':
    # parsing args
    parser = argparse.ArgumentParser()
    parser.add_argument('--regions', nargs='+', action='store', dest='regions', default=['NA', 'EU', 'AS', 'JP'],
                        help="A list of global regions to be included in the refresh. [Values] a list within"
                             " 'NA', 'EU', 'AS', 'JP'. [Default] all regions.")
    parser.add_argument('--dt', type=str, action='store', dest='dt', default=date.today().__str__(),
                        help="Date string for the refresh date. [Default] today's date. YYYY-MM-DD")
    parser.add_argument('--data_dir', type=str, action='store', dest='data_dir', default="data",
                        help="Relative location with respect to home_dir, indicating where to find all the "
                             "feature data.")
    parser.add_argument('--home_dir', type=str, action='store', dest='home_dir', default='.',
                        help="Parent location where the program runs and all sub-folders created within. "
                             "[Default] current location.")
    parser.add_argument('--output_dir', type=str, action='store', dest='output_dir', default='scores',
                        help="Relative location with respect to home_dir, indicating where to save all the "
                             "models. [Default] 'models'.")
    parser.add_argument('--logfilename', type=str, action='store', dest='logfile_name', default="log/fpa_model_score.log",
                        help="The file location for the log, with respect to home_dir. [Default] 'log/fpa_model_score.log'.")

    args = parser.parse_args()

    data_dir = os.path.join(os.path.realpath(args.home_dir), args.data_dir)
    output_dir = make_dir_if_not_exist(os.path.join(os.path.realpath(args.home_dir), args.output_dir))
    logfile_name = os.path.join(os.path.realpath(args.home_dir), args.logfile_name)

    # setup logger
    logFormatter = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=logFormatter, level=logging.INFO)
    logger = logging.getLogger()
    # os.remove(logfile_name) if os.path.exists(logfile_name) else None

    # add file handler
    handler = logging.FileHandler(logfile_name)
    handler.setFormatter(logging.Formatter(logFormatter))
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    try:
        for region in args.regions:
            logger.info(f"Processing {region}")
            output_dir_rg = os.path.join(output_dir, args.dt)
            data_dir_rg = os.path.join(data_dir, region, "score", args.dt)

            model, modelparams = reload_model("fpa_model_setup.yaml", region)
            logger.info(f"Loading {repr(model)} for scoring...")

            result_holder = list()
            for i, file_name in enumerate(glob.glob(os.path.join(data_dir_rg, "*.npz"))):
                logger.info(f"Processing {file_name} ...")
                X = prep_features_from_file_cnn(file_name, "train_X")
                with np.load(file_name, allow_pickle=True) as fh:
                    idx = fh["idx"]
                    data = pd.DataFrame(idx, columns=["rownum", "sk_goldencustomerid"], dtype=int)
                    # real = pd.DataFrame(fh["train_y"].squeeze(), columns=list(range(1, 22, 1)))
                _pred = model(X).squeeze().detach().numpy()
                pred = pd.DataFrame(_pred, columns=list(range(1, _pred.shape[-1]+1, 1)))
                result = pd.merge(data, pred, left_index=True, right_index=True)
                result_holder.append(result)

                # print(result)
                # print("all")
                # print(pd.DataFrame(np.histogram(result.iloc[:, 2:].max(axis=1))))
                # print(pd.DataFrame(np.histogram(real.max(axis=1))))
                # print("mid and high")
                # print(pd.DataFrame(np.histogram(result.loc[:, list(range(1, 22, 3)) + list(range(3, 22, 3))].max(axis=1))))
                # print(pd.DataFrame(np.histogram(real.loc[:, list(range(1, 22, 3)) + list(range(3, 22, 3))].max(axis=1))))

                # fig = plt.figure(figsize=(10,8))
                # plt.boxplot(result.iloc[:, 2:], showfliers=False)
                # plt.grid(axis="x")
                # plt.show()
            
            # combine scores
            score_raw = pd.concat(result_holder)
            cols = {
                1: 'handbags_high',
                2: 'handbags_low',
                3: 'handbags_mid',
                4: 'jewelry_high',
                5: 'jewelry_low',
                6: 'jewelry_mid',
                7: 'oth_high',
                8: 'oth_low',
                9: 'oth_mid',
                10: 'rtw_high',
                11: 'rtw_low',
                12: 'rtw_mid',
                13: 'shoes_high',
                14: 'shoes_low',
                15: 'shoes_mid',
                16: 'slg_high',
                17: 'slg_low',
                18: 'slg_mid',
                19: 'sport_high',
                20: 'sport_low',
                21: 'sport_mid'
            }

            # raw output
            score_raw = score_raw.rename(columns=cols).drop("rownum", axis=1).set_index("sk_goldencustomerid")
            
            # total score is rank of sum of ranks + highest score available
            score_decile = score_raw.transform(lambda x: pd.qcut(x, q=10, labels=False) + 1)
            all_score = np.round((score_raw.max(axis=1) + score_decile.sum(axis=1)).rank(pct=True, ascending=True, method="average"), 4)
            score_decile["fpa_score"] = all_score
            
            # adding dates and region
            score_raw.insert(0, "region", region)
            score_raw["refresh_dt"] = args.dt
            score_decile.insert(0, "region", region)
            score_decile["refresh_dt"] = args.dt

            save_dataframe(score_raw, output_dir_rg, f"{region}_fpa_score_raw.csv")
            save_dataframe(score_decile, output_dir_rg, f"{region}_fpa_score_ranked.csv")

    except Exception as e:
        logger.error(e, exc_info=True)
